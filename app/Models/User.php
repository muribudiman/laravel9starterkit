<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Fortify\TwoFactorAuthenticatable;
use Laravel\Jetstream\HasProfilePhoto;
use Laravel\Sanctum\HasApiTokens;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;

class User extends Authenticatable
{
    use HasApiTokens;
    use HasFactory;
    use HasProfilePhoto;
    use Notifiable;
    use TwoFactorAuthenticatable;

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'role',
        'is_active',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
        'two_factor_recovery_codes',
        'two_factor_secret'
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = [
        'logoUrl',
        'profile_photo_url',
		'formattedCreatedAt',
        'formattedUpdatedAt',
		'roleLabel',
		'isActiveLabel',
		'isAdmin',
        'isSuperAdmin',
        'isVendor',
        'isUser',
        'balanceFormated',
    ];

	public function user_detail()
    {
        return $this->hasOne(UserDetail::class, 'user_id');
    }

	public function getFormattedCreatedAtAttribute($value)
    {
        return Carbon::parse($this->created_at)->format('d M Y H:i:s');
    }
        
    public function getFormattedUpdatedAtAttribute($value)
    {
        return Carbon::parse($this->updated_at)->format('d M Y H:i:s');
    }
	
	public function getBalanceFormatedAttribute($value)
    {
        return generalMoney($this->balance);
    }

	public function getIsUserAttribute($value)
    {
        $permission = ['user', 'admin', 'super-admin'];
        if(in_array($this->role, $permission)) {
            return true;
        }

        return false;
    }

	public function getIsVendorAttribute($value)
    {
        $permission = ['vendor'];
        if(in_array($this->role, $permission)) {
            return true;
        }

        return false;
    }

    public function getIsAdminAttribute($value)
    {
        $permission = ['admin', 'super-admin'];
        if(in_array($this->role, $permission)) {
            return true;
        }

        return false;
    }
	
    public function getIsSuperAdminAttribute($value)
    {
        $permission = ['super-admin'];
        if(in_array($this->role, $permission)) {
            return true;
        }

        return false;
    }

	public function getLogoUrlAttribute($value)
    {
		$storageData = $this->user_detail->logo ?? 'null.jpg';
		if (Storage::disk('public')->exists("member_logo/{$storageData}")) {
			return asset("storage/member_logo/{$storageData}" . '?' . time());
		} else {
			return asset("images/logo_default.png"  . '?' . time());
		}
    }

	public function getRoleLabelAttribute()
    {
		$arrayData =  ['super-admin' => 'Super Admin', 'admin' => 'Admin', 'vendor' => 'Vendor', 'user' => 'Member'];
        return $arrayData[$this->role] ?? '';
    }

	public function getIsActiveLabelAttribute($value)
    {
        $arrayData =  [0 => 'Tidak Aktif', 1 => 'Aktif', 2 => 'Blocked'];
        return $arrayData[$this->is_active] ?? '';
    }
	
	public function scopeWhereIsActive($query, $search = true)
    {
        $query->where('users.is_active', $search);
    }
	
	public function scopeWhereRole($query, $search)
    {
        $query->where('users.role', $search);
    }

	public function scopeWhereEmailNotIn($query, $search = [])
    {
        $query->whereNotIn('users.email', $search);
    }

    public function scopeWhereRoleIn($query, $search = [])
    {
        $query->whereIn('users.role', $search);
    }

    public function scopeWhereSearch($query, $search)
    {
        foreach (explode(' ', $search) as $term) {
            $query->where('users.name', 'LIKE', '%'.$term.'%')
                ->orWhere('users.email', 'LIKE', '%'.$term.'%');
        }
    }
    
    public function scopeApplyFilters($query, array $filters)
    {
        $filters = collect($filters);
        if ($filters->get('search')) {
            $query->whereSearch($filters->get('search'));
        }
    }

    public function scopePaginateData($query, $limit)
    {
        if ($limit == 'all') {
            return collect(['data' => $query->get()]);
        }

        return $query->paginate($limit);
    }

	public static function createWebApp($request) {        
        $data = $request->validated();

        $data['password'] = Hash::make($data['password']);

        $user = self::create($data);
		$user->fresh();
		if(isset($data['role']) && $data['role'] == 'vendor') {
			$user->user_detail()->create($data);
			if($request->hasFile('picture'))
			{
				$file = $request->file('picture');
				$filename = md5($user->id . time()) . '.' . $file->getClientOriginalExtension();
				$file->storeAs('public/member_logo/', $filename);
				$user->user_detail()->update([
					'logo' => $filename
				]);
			}
		}

        return $user;
    }

    public function updateWebApp($request) {
        $data = $request->validated();
        
		if(isset($data['password']) && $data['password']) {
			$data['password'] = Hash::make($data['password']);
		} else {
			unset($data['password']);
		}
        
        if($this->update($data)) {
			if($this->role == "vendor") {
				$this->user_detail()->delete();
				$this->user_detail()->create($data);

				if($request->hasFile('picture')) {			
					Storage::disk('public')->delete("member_logo/{$this->user_detail->logo}");				
					$file = $request->file('picture');
					$filename = md5($this->id . time()) . '.' . $file->getClientOriginalExtension();
					$file->storeAs('public/member_logo/', $filename);
					$this->user_detail()->update([
						'logo' => $filename
					]);
				}
			}
		}

        return $this;
    }

    public static function createUser($request) {        
        $data = $request->validated();

        $data['password'] = Hash::make($data['password']);

        $user = self::create($data);

		if(isset($data['role']) && $data['role'] == 'vendor') {
			$user->user_detail()->create($data);
		}

        return $user;
    }

    public function updateUser($request) {
        $data = $request;
        
		if(isset($data['password']) && $data['password']) {
			$data['password'] = Hash::make($data['password']);
		} else {
			unset($data['password']);
		}
        
        if($this->update($data)) {
			if($this->role == "vendor") {
				$this->user_detail()->delete();
				$this->user_detail()->create($data);
			}
		}

        return $this;
    }

	public static function logout($request) {
		$replace = str_replace("Bearer ", "", $request->header('authorization'));
		$explode = explode("|", $replace);
		
		if($request->input('type') == 'all') {
			Auth::user()->tokens()->delete();
		} else {
			if(isset($explode[0])) {
				Auth::user()->tokens()->where('id', $explode[0])->delete();
			}
		}
	}

}
