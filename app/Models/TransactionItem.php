<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Traits\Uuid;
use Carbon\Carbon;

class TransactionItem extends Model
{
    use HasFactory, Uuid;

	/**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'transaction_id',
        'product_id',
        'product_name',
        'qty',
        'price',
        'total',
    ];

	public $timestamps = true;

    public $incrementing = false;

	public function product()
    {
		return $this->hasOne(Product::class, 'id', 'product_id');
    }

}
