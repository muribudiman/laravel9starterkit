<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Auth;
use App\Traits\Uuid;
use Carbon\Carbon;

class UserDetail extends Model
{
    use HasFactory, Uuid;

	protected $fillable = [
        'user_id',
        'company_name',
        'leader_name',
        'address',
        'phone',
        'mobile',
        'lat',
        'lng'
    ];

    public $timestamps = true;

    public $incrementing = false;

	/**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = [
        'logoUrl',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

	public function user()
    {
        return $this->belongsTo(Users::class, 'user_id');
    }

	public function getLogoUrlAttribute($value)
    {
		$storageData = $this->logo ?? 'null.jpg';
		if (Storage::disk('public')->exists("member_logo/{$storageData}")) {
			return asset("storage/member_logo/{$storageData}" . '?' . time());
		} else {
			return asset("images/logo_default.png"  . '?' . time());
		}
    }

	public static function updateDetail($request) {
        $data = $request->validated();
		$user = Auth::user();
		$model = UserDetail::where('user_id', $user->id ?? 0)->first();
		
		if($model->exists() && $model->update($data)) {
			if($request->hasFile('logo')) {			
				Storage::disk('public')->delete("member_logo/{$model->logo}");				
				$file = $request->file('logo');
				$filename = md5($model->id . time()) . '.' . $file->getClientOriginalExtension();
				$file->storeAs('public/member_logo/', $filename);
				$model->logo = $filename;
				$model->save();
			}
		} else {
			if($new = UserDetail::create($data)) {
				$new->logo = $new->logo;
				$new->save();

				if($request->hasFile('logo')) {			
					Storage::disk('public')->delete("member_logo/{$new->logo}");				
					$file = $request->file('logo');
					$filename = md5($new->id . time()) . '.' . $file->getClientOriginalExtension();
					$file->storeAs('public/member_logo/', $filename);
					$new->logo = $filename;
					$new->save();
				}
			}
		}

		return false;
    }
	
}
