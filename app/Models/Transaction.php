<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use App\Traits\Uuid;
use Carbon\Carbon;

class Transaction extends Model
{
    use HasFactory, Uuid;

	/**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'vendor_id',
        'member_id',
        'transaction_number',
        'ref_number',
        'total',
        'type',
    ];

	public $timestamps = true;

    public $incrementing = false;

	/**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = [
		'formattedCreatedAt',
        'formattedUpdatedAt',
        'transactionCode',
        'debit',
        'kredit',
    ];

	public function items()
    {
		return $this->hasMany(TransactionItem::class, 'transaction_id', 'id');
    }
	
	public function member()
    {
		return $this->hasOne(User::class, 'id', 'member_id');
    }

	public function getFormattedCreatedAtAttribute($value)
    {
        return Carbon::parse($this->created_at)->format('d M Y H:i:s');
    }
        
    public function getFormattedUpdatedAtAttribute($value)
    {
        return Carbon::parse($this->updated_at)->format('d M Y H:i:s');
    }

	public function getDebitAttribute($value)
    {
		$debit = null;
        if($this->type && $this->type == 'D') {
			$debit = generalMoney($this->total);
		}

		return $debit;
    }

	public function getKreditAttribute($value)
    {
		$kredit = null;
        if($this->type && $this->type == 'K') {
			$kredit = generalMoney($this->total);
		}

		return $kredit;
    }
	
	public function getTransactionCodeAttribute($value)
    {
		return self::transactionNumberFormat($this->transaction_number ?? 0);
    }

	public static function transactionNumberFormat($number)
    {
		$numberLength = 5;
        $numberLengthText = "%0{$numberLength}d";

		return sprintf($numberLengthText, intval($number));
	}

	public static function getNextTransactionNumber()
    {
        $lastOrder = Transaction::where('vendor_id', Auth::check() ? Auth::id() : 0)
            ->orderBy('transaction_number', 'desc')
            ->first();

        $numberLength = 5;
        $numberLengthText = "%0{$numberLength}d";

        if (! $lastOrder) {
            $number = 0;
        } else {
            //$number = explode("-", $lastOrder->transaction_number);
            $number = $lastOrder->transaction_number;
        }

        return sprintf($numberLengthText, intval($number) + 1);
    }

	public function scopeWhereMe($query)
    {
        $query->where('vendor_id', Auth::check() ? Auth::id() : 0);
    }

	public function scopeWhereSearch($query, $search)
    {
        foreach (explode(' ', $search) as $term) {
            $query->where('users.name', 'LIKE', '%'.$term.'%')
                ->orWhere('users.email', 'LIKE', '%'.$term.'%');
        }
    }
    
    public function scopeApplyFilters($query, array $filters)
    {
        $filters = collect($filters);
        if ($filters->get('search')) {
            $query->whereSearch($filters->get('search'));
        }
    }

    public function scopePaginateData($query, $limit)
    {
        if ($limit == 'all') {
            return collect(['data' => $query->get()]);
        }

        return $query->paginate($limit);
    }

	public static function createTransactionWeb($request) {
		$data = $request;
		$user_id = Auth::check() ? Auth::id() : false;

		if($user_id) {
			$data['vendor_id'] = $user_id;
			$data['transaction_number'] = self::getNextTransactionNumber();
			$data['type'] = 'K';
			$data['total'] = 0;
			
			$items = $data['detail'];
			if($model = Transaction::create($data)) {
				$model->total = 0;
				$model->save();

				$grand_total = 0;
				foreach($items as $value) {
					$total = $value['quantity'] * $value['price'];
					$grand_total += $total;
					$item = [
						'transaction_id' => $model->id,
						'product_id' => $value['product_id'],
						'product_name' => "Bank Sampah Test",
						'qty' => $value['quantity'],
						'price' => $value['price'],
						'total' => $total,
					];

					TransactionItem::create($item);
				}

				$model->total = $grand_total;
				$model->save();

				$member = User::find($model->member_id);
				$member->balance = $member->balance + $model->total;
				$member->save();
			}

			return true;
		}

		return false;
        
	}

	public function updateTransactionWeb($request) {
		$data = $request;
		$data['total'] = 0;
		
		$total = $this->total;

		$items = $data['detail'];
		if($this->update($data)) {
			$grand_total = 0;
				TransactionItem::where('transaction_id', $this->id)->delete();

				foreach($items as $value) {
					$total = $value['quantity'] * $value['price'];
					$grand_total += $total;
					$item = [
						'transaction_id' => $this->id,
						'product_id' => $value['product_id'],
						'product_name' => "Bank Sampah Test",
						'qty' => $value['quantity'],
						'price' => $value['price'],
						'total' => $total,
					];

					TransactionItem::create($item);
				}

				$this->total = $grand_total;
				$this->save();

				$member = User::find($this->member_id);
				$member->balance = ($member->balance - $total)+ $this->total;
				$member->save();
		}

        return $this;

	}

	public static function createWithdrawalWeb($request) {
		$data = $request;
		$user_id = Auth::check() ? Auth::id() : false;
		$member = User::find($data['member_id']);
		if($user_id && ($member->balance >= $data['total']) ) {
			$data['vendor_id'] = $user_id;
			$data['transaction_number'] = self::getNextTransactionNumber();
			$data['type'] = 'D';
			
			if($model = Transaction::create($data)) {
				$model->total = $model->total;
				$model->save();

				
				$member->balance = $member->balance - $model->total;
				$member->save();
			}

			return true;
		}

		return false;
        
	}
}
