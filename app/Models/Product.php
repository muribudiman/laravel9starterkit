<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Product extends Model
{
    use HasFactory;

	/**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'category_id',
        'vendor_id',
        'name',
        'desc',
        'price',
        'discount',
        'stock',
        'is_active',
        'is_delete',
    ];

	/**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = [
		'formattedCreatedAt',
        'formattedUpdatedAt',
        'isActiveLabel',
    ];

	public function category()
    {
        return $this->hasOne(Category::class, 'id', 'category_id');
    }

	/** membuat default format tanggal create */
	public function getFormattedCreatedAtAttribute($value)
    {
        return Carbon::parse($this->created_at)->format('d M Y H:i:s');
    }
    
	/** membuat default format tanggal update */
    public function getFormattedUpdatedAtAttribute($value)
    {
        return Carbon::parse($this->updated_at)->format('d M Y H:i:s');
    }

	public function getIsActiveLabelAttribute($value)
    {
        $arrayData =  [0 => 'Tidak Aktif', 1 => 'Aktif'];
        return $arrayData[$this->is_active] ?? '';
    }

	public function scopeWhereIsActive($query, $search = true)
    {
        $query->where('products.is_active', $search);
    }

	/**
	 * name : sesuai yang di tabel
	 * desc : sesuai yang di tabel
	 */
	public function scopeWhereSearch($query, $search)
    {
        foreach (explode(' ', $search) as $term) {
            $query->where('products.name', 'LIKE', '%'.$term.'%')
                ->orWhere('products.desc', 'LIKE', '%'.$term.'%');
        }
    }

	/**
	 * name : sesuai yang di tabel
	 * desc : sesuai yang di tabel
	 */
	public function scopeWhereMe($query)
    {
        $query->where('vendor_id', Auth::check() ? Auth::id() : 0);
    }
    
    public function scopeApplyFilters($query, array $filters)
    {
        $filters = collect($filters);
        if ($filters->get('search')) {
            $query->whereSearch($filters->get('search'));
        }
    }

	public function scopePaginateData($query, $limit)
    {
        if ($limit == 'all') {
            return collect(['data' => $query->get()]);
        }

        return $query->paginate($limit);
    }

	public static function createModel($request) {        
        $data = $request;
        $data['vendor_id'] = Auth::check() ? Auth::id() : null;

        $model = self::create($data);

        return $model;
    }

    public function updateModel($request) {
        $data = $request;
        $model = $this->update($data);

        return $model;
    }

}
