<?php

function dashboardGetColor($num) {
    $hash = md5('color' . $num); // modify 'color' to get a different palette
    return array(
        hexdec(substr($hash, 0, 2)), // r
        hexdec(substr($hash, 2, 2)), // g
        hexdec(substr($hash, 4, 2)),
	); //b
}

function dashboardLineChart($data = [], $period=[]) {
	$labels = [];
	$gb_in = [];
	$gb_out = [];
	$backgroundColor = [];
	$borderColor = [];

	$backgroundColorOut = [];
	$borderColorOut = [];

	$datax = [];
	foreach ($period as $date)
	{
		$datax["{$date->year}_{$date->month}"] = [
			'name' => "{$date->year}-{$date->shortMonthName}",
			'month' => $date->month,
			'year' => $date->year,
		];
	}

	$dataQuery = [];
	foreach($data as $value) {
		$dataQuery["{$value['y']}_{$value['m']}"] = [
			'shortMonthName' => $date->monthName,
			'month' => $value['month'],
			'gb_in' => $value['gb_in'],
			'gb_out' => $value['gb_out'],
		];
	}

	foreach($datax as $key => $value) {
		if(isset($dataQuery[$key])) {
			$labels[] = $value['name'];
			$gb_in[] = $dataQuery[$key]['gb_in'];
			$gb_out[] = $dataQuery[$key]['gb_out'];
		} else {
			$labels[] = $value['name'];
			$gb_in[] = 0;
			$gb_out[] = 0;
		}
		
		$backgroundColor[] = 'rgba(75, 192, 192, 0.2)';
		$borderColor[] = 'rgba(75, 192, 192, 1)';

		$backgroundColorOut[] = 'rgba(255, 159, 64, 0.2)';
		$borderColorOut[] = 'rgba(255, 159, 64, 1)';
	}

	$dataC = [
		'labels' => $labels,
		'datasets' => [
			[
				'label' => 'GB IN',
				'data' => $gb_in,
				'backgroundColor' => $backgroundColor,
				'borderColor' => $borderColor,
				'borderWidth' => 2,
			],
			[
				'label' => 'GB OUT',
				'data' => $gb_out,
				'backgroundColor' => $backgroundColorOut,
				'borderColor' => $borderColorOut,
				'borderWidth' => 2,
			],
		],
	];

	return $dataC;

}