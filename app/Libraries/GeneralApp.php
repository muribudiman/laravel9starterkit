<?php

function generalMoney($number = 0, $format = 'IDR.') {
	return $format . ' ' . number_format($number, 0, ',', '.');
}

function generalFormatDistance($distance = null) {
	$returnValue = "";
	if($distance == null) {
		$returnValue = "-";
	} else {
		if($distance < 1) {
			$returnValue = ((String) round(($distance * 1000), 1)) . " Meter";
		} else {
			$returnValue = (String) round(($distance), 1). " Kilometer";
		}
	}

	return $returnValue;
}

function generalInputText($string = "") {
	return strip_tags(trim(ucwords($string)));
}

function generalGetArrayId($array = []) {
	$newArray = [];
	foreach($array as $value) {
		if($value['id'] !== '') {
			$newArray[$value['id']] = $value['name'];
		}
	}

	return $newArray;
}

function generalFormatArrayInput($array = []) {
	$newArray = [['id' => null, 'name' => 'None']];
	foreach($array as $value) {
		$newArray[] = $value;
	}

	return $newArray;
}

function generalStatus() {
	return [
		[
			'id' => '',
			'name' => 'None'
		],
		[
			'id' => 'active',
			'name' => 'Active'
		],
		[
			'id' => 'inactive',
			'name' => 'Inactive'
		],
	];
}

function roleApiTranslate($role) {
	$statusTranslate = "user";
	switch ($role) {
		case "user":
			$statusTranslate = "user";
			break;
		case "bank":
			$statusTranslate = "bank";
			break;
		case "super-admin":
			$statusTranslate = "sx";
			break;
		case "admin":
			$statusTranslate = "sy";
			break;
		default:
			$statusTranslate = "user";
	}

	return $statusTranslate;
}