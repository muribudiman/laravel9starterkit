<?php

namespace App\Http\Controllers\Jetstream\Dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Inertia\Inertia;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class VendorController extends Controller
{
    /**
     * Retrieve details of an expense receipt from storage.
     *
     * @param   \Crater\Models\Expense $expense
     * @return  \Illuminate\Http\JsonResponse
     */
    public function __invoke(Request $request)
    {
		$from = Carbon::now()->subYear()->format('Y-m') . '-01';
		$to = Carbon::now()->format('Y-m-d');
		
		$period = Carbon::now()->subMonths(11)->monthsUntil(now());

		//config()->set('database.connections.mysql.strict', false);
		//DB::reconnect();
		
		//config()->set('database.connections.mysql.strict', true);
		//DB::reconnect();
		
        return Inertia::render('Vendor/Dashboard/Index', [
            'dataUsageByPeriod' => dashboardLineChart([], $period),
        ]);
    }
}
