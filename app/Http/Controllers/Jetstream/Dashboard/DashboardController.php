<?php

namespace App\Http\Controllers\Jetstream\Dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Inertia\Inertia;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class DashboardController extends Controller
{
    /**
     * Retrieve details of an expense receipt from storage.
     *
     * @param   \Crater\Models\Expense $expense
     * @return  \Illuminate\Http\JsonResponse
     */
    public function __invoke(Request $request)
    {
		if(Auth::user()->isAdmin || Auth::user()->isVendor) {
			$from = Carbon::now()->subYear()->format('Y-m') . '-01';
			$to = Carbon::now()->format('Y-m-d');
			
			$period = Carbon::now()->subMonths(11)->monthsUntil(now());

			//config()->set('database.connections.mysql.strict', false);
			//DB::reconnect();
			
			//config()->set('database.connections.mysql.strict', true);
			//DB::reconnect();
			
			return Inertia::render('Dashboard/Index', [
				'dataUsageByPeriod' => dashboardLineChart([], $period),
			]);
    	} else {
			abort(404);
		}
    }
}
