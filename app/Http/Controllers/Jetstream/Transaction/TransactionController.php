<?php

namespace App\Http\Controllers\Jetstream\Transaction;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\Transaction\TransactionRequest;
use App\Models\Transaction;
use Inertia\Inertia;

class TransactionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $limit = (int) $request->get('per_page') > 0 ? (int) $request->get('per_page') : 15;
        $page = (int) $request->get('page') > 0 ? (int) $request->get('page') : 1;

        $queries = ['search', 'page'];

        return Inertia::render('Transaction/In/Index', [
            'models' => Transaction::with(['member'])
				->whereMe()
				->applyFilters($request->only($queries))
				->orderBy('transaction_number', "DESC")
                ->paginateData($limit),
            'filters' => $request->all($queries),
            'start' => $limit * ($page - 1),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
		$transaction_number = Transaction::getNextTransactionNumber();

        return Inertia::render('Transaction/In/Create', [
			'trx' => $transaction_number
		]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(TransactionRequest $request)
    {
		if(Transaction::createTransactionWeb($request->validated())) {
			return redirect()->route('transactions.index')->with('success', 'Success');
		}
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Transaction $transaction)
    {
		$transaction->load([
			'member',
			'items',
			'items.product'
		]);
		
		return Inertia::render('Transaction/In/Edit', [
			'model' => $transaction
		]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(TransactionRequest $request, Transaction $transaction)
    {
        if($this->updateTransactionWeb($request->validated())) {
			return redirect()->route('transactions.index')->with('success', 'Success');
		}
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
