<?php

namespace App\Http\Controllers\Jetstream\Product;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\Products\Product\ProductRequest;
use Illuminate\Http\Request;
use App\Models\Category;
use App\Models\Product;

use Inertia\Inertia;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $limit = (int) $request->get('per_page') > 0 ? (int) $request->get('per_page') : 15;
        $page = (int) $request->get('page') > 0 ? (int) $request->get('page') : 1;

        $queries = ['search', 'page'];

        return Inertia::render('Products/Product/Index', [
            'models' => Product::with(['category'])
				->applyFilters($request->only($queries))
				->whereMe()
                ->paginateData($limit),
            'filters' => $request->all($queries),
            'start' => $limit * ($page - 1),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
		$category = Category::select('id as value', 'name')->paginateData("all")->toArray();
		$categoryFormat = generalFormatArrayInput($category['data']);

        return Inertia::render('Products/Product/Create', [
			'categories' => $categoryFormat
		]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProductRequest $request)
    {
        Product::createModel($request->validated());

        return redirect()->route('products.index')->with('success', 'Success');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
		$auth_id = Auth::check() ? Auth::id() : 0;
		if($auth_id !== $product->vendor_id) {
			abort(404);
		}

		$category = Category::select('id as value', 'name')->paginateData("all")->toArray();
		$categoryFormat = generalFormatArrayInput($category['data']);

        return Inertia::render('Products/Product/Edit', [
			'model' => $product,
			'categories' => $categoryFormat
		]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ProductRequest $request, Product $product)
    {
		$auth_id = Auth::check() ? Auth::id() : 0;
		if($auth_id !== $product->vendor_id) {
			abort(404);
		}
		
        $product->updateModel($request->validated());
        return redirect()->route('products.index')->with('success', 'Success');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
