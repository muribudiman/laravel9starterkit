<?php

namespace App\Http\Controllers\Jetstream\Api\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;

class UserController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @return Json
     */
    public function __invoke(Request $request)
    {
		try {
			$limit = (int) $request->get('per_page') > 0 ? (int) $request->get('per_page') : 15;
			$queries = ['search', 'page'];

            $model = User::applyFilters($request->only($queries))
				->whereRoleIn(['super-admin', 'admin', 'user'])
				->whereIsActive(true)
				->paginateData($limit);

            $success = api_format(true, [], $model->items(), []);
            return response()->json($success, 200);
        } catch (\Exception $ex) {
            $error = api_format(false, [$ex->getMessage()], [], []);
            return response()->json($error, 200);
        }
	}
}
