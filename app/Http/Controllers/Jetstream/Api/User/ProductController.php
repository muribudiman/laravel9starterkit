<?php

namespace App\Http\Controllers\Jetstream\Api\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Product;

class ProductController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @return Json
     */
    public function __invoke(Request $request)
    {
		try {
			$limit = (int) $request->get('per_page') > 0 ? (int) $request->get('per_page') : 15;
			$queries = ['search', 'page'];

            $model = Product::applyFilters($request->only($queries))
				->whereMe()
				->whereIsActive(true)
				->paginateData($limit);

            $success = api_format(true, [], $model->items(), []);
            return response()->json($success, 200);
        } catch (\Exception $ex) {
            $error = api_format(false, [$ex->getMessage()], [], []);
            return response()->json($error, 200);
        }
	}
}
