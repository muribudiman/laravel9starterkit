<?php

namespace App\Http\Controllers\Jetstream\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\UserDetail;
use App\Http\Requests\Users\Vendor\UserDetailRequest;

class UserDetailController extends Controller
{
    /**
     * Retrieve details of an expense receipt from storage.
     *
     * @param   \Crater\Models\Expense $expense
     * @return  \Illuminate\Http\JsonResponse
     */
    public function __invoke(UserDetailRequest $request)
    {
		try {
			UserDetail::updateDetail($request);
			return redirect()->back()->with('success', 'Success');
		} catch (\Exception $ex) {

		}
    }
}
