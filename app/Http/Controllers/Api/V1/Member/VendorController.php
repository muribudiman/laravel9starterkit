<?php

namespace App\Http\Controllers\Api\V1\Member;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\User;
use Illuminate\Support\Facades\Lang;

class VendorController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        try {
			// -7.767417005846905, 110.47260716206176
			$limit = (int) $request->get('per_page') > 0 ? (int) $request->get('per_page') : 15;
			$centerLat = $request->has('lat') ? (double) $request->get('lat') : (double) 0.0;
			$centerLng = $request->has('lng') ? (double) $request->get('lng'): (double) 0.0;

            $model = User::leftJoin('user_details', 'user_details.user_id', '=', 'users.id')
						->applyFilters($request->only([
                            'search',
                        ]))
						->whereRole('vendor')
						->select('users.*',
							DB::raw("(( 3958.8  *
								acos(
										cos( radians({$centerLat}) ) *
										cos( radians( user_details.lat ) ) *
										cos( radians( user_details.lng ) - radians({$centerLng}) ) +
										sin( radians({$centerLat})) *
										sin( radians( user_details.lat ) )
									)
							))
							AS distance"))
                        ->orderBy("distance", "ASC")
                        ->paginateData($limit);
						
			foreach($model as $key => $value) {
				$model[$key]['distanceString'] = generalFormatDistance($value->distance);
			}
            
            $paginate = [
                'total' => (int) $model->total(),
                'currentPage' => (int) $model->currentPage(),
                'lastPage' => (int) $model->lastPage(),
                'hasMorePages' => (boolean) $model->hasMorePages(),
                'perPage' => (int) $model->perPage(),
                'total' => (int) $model->total(),
                'lastItem' => (int) $model->lastItem(),
            ];

            $success = api_format(true, [], $model->items(), $paginate);
            return response()->json($success, 200);
        } catch (\Exception $ex) {
            $success = api_format(false, [["message" => [$ex->getMessage()]]], [], []);
            return response()->json($success, 200);
        }
    }
}
