<?php

namespace App\Http\Controllers\Api\V1\Member;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Lang;

class MemberController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        try {
			$limit = (int) $request->get('per_page') > 0 ? (int) $request->get('per_page') : 15;

            $model = User::select('users.id', 'users.name', 'users.email')
						->applyFilters($request->only([
                            'search',
                        ]))
						->whereRole('user')
                        ->paginateData($limit);
            
            $paginate = [
                'total' => (int) $model->total(),
                'currentPage' => (int) $model->currentPage(),
                'lastPage' => (int) $model->lastPage(),
                'hasMorePages' => (boolean) $model->hasMorePages(),
                'perPage' => (int) $model->perPage(),
                'total' => (int) $model->total(),
                'lastItem' => (int) $model->lastItem(),
            ];

            $success = api_format(true, [], $model->items(), $paginate);
            return response()->json($success, 200);
        } catch (\Exception $ex) {
            $success = api_format(false, [], [], []);
            return response()->json($success, 200);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
				'name' => 'required|string|max:255',
                'email' => 'required|email|max:255|unique:users',
                'password' => 'required|string|min:8',
            ])->setAttributeNames([
                'name' => 'Nama',
                'email' => 'Email',
                'password' => 'Password',
            ]);
            
            if ($validator->fails()) {
                $error = api_format(false, [$validator->errors()->toArray()], [], []);
                return response()->json($error, 200);
            } else {
                User::createUser($request->only(['name', 'email', 'role', 'password']));

                $success = api_format(true, [["msg" => [Lang::get('messages.message_create', ['attribute' => "Data Member"])]]], [], []);
				return $success;
            }
        } catch (\Exception $ex) {
            $success = api_format(false, [["message" => [$ex->getMessage()]]], [], []);
            return response()->json($success, 200);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $member)
    {
        try {
            $validator = Validator::make($request->all(), [
				'name' => 'required|string|max:255',
                'email' => 'required|email|max:255|unique:users,email,' . $member->id,
                'password' => 'nullable|string|min:8',
            ])->setAttributeNames([
                'name' => 'Nama',
                'email' => 'Email',
                'password' => 'Password',
            ]);
            
            if ($validator->fails()) {
                $error = api_format(false, [$validator->errors()->toArray()], [], []);
                return response()->json($error, 200);
            } else {
                $member->updateUser($request->only(['name', 'email', 'role', 'password']));

                $success = api_format(true, [["msg" => [Lang::get('messages.message_update', ['attribute' => "Data Member"])]]], [], []);
				return $success;
            }
        } catch (\Exception $ex) {
            $success = api_format(false, [["message" => [$ex->getMessage()]]], [], []);
            return response()->json($success, 200);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
