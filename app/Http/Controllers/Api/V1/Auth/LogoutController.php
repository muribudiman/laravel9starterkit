<?php

namespace App\Http\Controllers\Api\V1\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Lang;

class LogoutController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        try {
            
            $validator = Validator::make($request->only(['type']), [
                'type' => 'required|in:all,active',
            ]);
            
            if ($validator->fails()) {
                $error = api_format(false, [$validator->errors()->toArray()], [], []);
                return response()->json($error, 200);
            } else {
                if(User::logout($request)) {
					$success = api_format(true, [["msg" => [Lang::get('messages.api_logout')]]], [], []);
            		return response()->json($success, 200);
				}
                
                $success = api_format(true, [["msg" => [Lang::get('messages.api_logout_false')]]], [], []);
            	return response()->json($success, 200);
            }
        } catch (\Exception $ex) {
            $success = api_format(false, [["message" => [$ex->getMessage()]]], [], []);
            return response()->json($success, 200);
        }
    }
}
