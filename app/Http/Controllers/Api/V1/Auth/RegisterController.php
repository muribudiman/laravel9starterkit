<?php

namespace App\Http\Controllers\Api\V1\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Lang;
use Illuminate\Http\JsonResponse;

class RegisterController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request): JsonResponse
    {
        try {
            $validator = Validator::make($request->all(), [
				'name' => 'required|string|max:255',
                'email' => 'required|email|max:255|unique:users',
				'role' => 'required|string|in:user',
                'password' => 'required|string|min:8|confirmed',
                'password_confirmation' => 'required|string|min:8',
            ])->setAttributeNames([
                'name' => 'Nama',
                'email' => 'Email',
                'password' => 'Password',
                'password_confirmation' => 'Konfirmasi Password',
            ]);
            
            if ($validator->fails()) {
                $error = api_format(false, [$validator->errors()->toArray()], [], []);
                return response()->json($error, 200);
            } else {
                User::createUser($request->only(['name', 'email', 'role', 'password']));

                $success = api_format(true, [["msg" => [Lang::get('messages.message_create', ['attribute' => "Data Member"])]]], [], []);
				return $success;
            }
        } catch (\Exception $ex) {
            $success = api_format(false, [["message" => [$ex->getMessage()]]], [], []);
            return response()->json($success, 200);
        }
    }
}
