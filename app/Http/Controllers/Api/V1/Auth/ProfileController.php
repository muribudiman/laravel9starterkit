<?php

namespace App\Http\Controllers\Api\V1\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\User;

class ProfileController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @return Json
     */
    public function __invoke()
    {
		try {
            $model = User::whereId((Auth::check() ? Auth::id() : null))
					->select('users.name', 'users.email', 'users.role', 'users.balance')
                    ->first();

            $success = api_format(true, [], [$model->toArray()], []);
            return response()->json($success, 200);
        } catch (\Exception $ex) {
            $error = api_format(false, [$ex->getMessage()], [], []);
            return response()->json($error, 200);
        }
	}
}
