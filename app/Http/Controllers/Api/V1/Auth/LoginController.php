<?php

namespace App\Http\Controllers\Api\V1\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Lang;

class LoginController extends Controller
{

    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        try {
            
            $validator = Validator::make($request->only(['email', 'password', 'device_name']), [
                'email' => 'required|email|exists:users',
                'password' => 'required',
                'device_name' => 'required|max:150',
            ]);
            
            if ($validator->fails()) {
                $error = api_format(false, [$validator->errors()->toArray()], [], []);
                return response()->json($error, 200);
            } else {
                $model = User::where('email', $request->input('email'))->first();
                if($model && Hash::check($request->input('password'), $model->password)) {
					if($model->is_active === 1) {
						$loginData = [
							'id' => $model->id,
							'name' => $model->name,
							'email' => $model->email,
							'role' => roleApiTranslate($model->role),
							'type' => 'Bearer',
							'token' => $model->createToken($request->device_name)->plainTextToken,
						];
						
						$success = api_format(true, [["msg" => [Lang::get('messages.api_login')]]], [$loginData], []);
						
						return response()->json($success, 200);
					} else {
						$status = $model->is_active == 0 ? "Tidak Aktif" : "diblokir";
						$message = Lang::get('messages.contact_administrator');
						$errorlogin = [
							"email" => [Lang::get('messages.account_status', ['status' => $status, 'message' => $message])]
						];

						$success = api_format(false, [$errorlogin], [], []);
						return response()->json($success, 200);
					}
                } 
                
                $errorlogin = [
                    "email" => [Lang::get('messages.login_failed')]
                ]; 
                
                $success = api_format(false, [$errorlogin], [], []);
                return response()->json($success, 200);
            }
        } catch (\Exception $ex) {
            $success = api_format(false, [["message" => [$ex->getMessage()]]], [], []);
            return response()->json($success, 200);
        }
    }

}
