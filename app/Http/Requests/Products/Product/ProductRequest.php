<?php

namespace App\Http\Requests\Products\Product;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class ProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.s
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'category_id' => [
                'required',
                'exists:categories,id',
            ],
			'name' => [
                'required',
                'max:200',
            ],
            'desc' => [
                'required',
                'max:65535',
            ],
			'price' => [
                'required',
                'numeric',
                'min:0',
            ],
			'stock' => [
                'required',
                'numeric',
                'min:0',
            ],
			'discount' => [
                'required',
                'numeric',
                'min:0',
                'max:100',
            ],
            'is_active' => [
                'required',
                'in:1,0',
            ],
        ];

        if ($this->getMethod() == 'PUT') {
            //$rules['name'] = [
            //    'required',
            //    Rule::unique('categories')->ignore($this->category),
            //];
        }

        return $rules;
    }

    /**
    * Get custom attributes for validator errors.
    *
    * @return array
    */
    public function attributes()
    {
        $attributes = [
            'category_id' => 'Kategori',
            'name' => 'Nama Produk',
            'price' => 'Harga',
            'discount' => 'Diskon',
            'stock' => 'Stok/Ketersediaan',
            'desc' => 'Keterangan',
            'is_active' => 'Status',
        ];

        return $attributes;
    }
}
