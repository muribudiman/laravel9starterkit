<?php

namespace App\Http\Requests\Products\Category;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class CategoryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.s
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'name' => [
                'required',
                'max:100',
                'unique:categories',
            ],
            'desc' => [
                'required',
                'max:255',
            ],
            'is_active' => [
                'required',
                'in:1,0',
            ],
        ];

        if ($this->getMethod() == 'PUT') {
            $rules['name'] = [
                'required',
                Rule::unique('categories')->ignore($this->category),
            ];
        }

        return $rules;
    }

    /**
    * Get custom attributes for validator errors.
    *
    * @return array
    */
    public function attributes()
    {
        $attributes = [
            'name' => 'Kategori',
            'desc' => 'Keterangan',
            'is_active' => 'Status',
        ];

        return $attributes;
    }
}
