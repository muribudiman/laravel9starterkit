<?php

namespace App\Http\Requests\Transaction;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class WithdrawalRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.s
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'member_id' => [
                'required',
                'exists:users,id',
            ],
            'ref_number' => [
                'nullable',
                'max:255',
            ],
			'total' => [
                'required',
                'numeric',
                'max:5000000',
            ],
        ];

        if ($this->getMethod() == 'PUT') {
            //
        }

        return $rules;
    }

    /**
    * Get custom attributes for validator errors.
    *
    * @return array
    */
    public function attributes()
    {
        $attributes = [
            'member_id' => 'Anggota',
            'ref_number' => 'Nomor Referensi',
            'total' => 'Total Penarikan',
        ];

        return $attributes;
    }
}
