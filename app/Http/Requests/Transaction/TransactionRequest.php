<?php

namespace App\Http\Requests\Transaction;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class TransactionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.s
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'member_id' => [
                'required',
                'exists:users,id',
            ],
            'ref_number' => [
                'nullable',
                'max:255',
            ],
            'detail' => [
                'required',
                'array',
                'min:1',
            ],
			'detail.*.product_id' => [
				'required',
                'exists:products,id',
            ],
			'detail.*.quantity' => [
				'required',
                'numeric',
                'min:1',
            ],
			'detail.*.price' => [
				'required',
                'numeric',
                'min:1',
            ],
        ];

        if ($this->getMethod() == 'PUT') {
            //
        }

        return $rules;
    }

    /**
    * Get custom attributes for validator errors.
    *
    * @return array
    */
    public function attributes()
    {
        $attributes = [
            'member_id' => 'Anggota',
            'ref_number' => 'Nomor Referensi',
            'detail' => 'Item',
            'detail.*.product_id' => 'Produk',
            'detail.*.quantity' => 'Jumlah',
            'detail.*.price' => 'Harga',
        ];

        return $attributes;
    }
}
