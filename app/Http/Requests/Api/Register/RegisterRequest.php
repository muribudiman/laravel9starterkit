<?php

namespace App\Http\Requests\Api\Register;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Validation\ValidationException;

class RegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.s
     *
     * @return array
     */
    public function rules()
    {
		$this->merge(['role' => 'user']);

        $rules = [
            'name' => [
                'required',
                'max:255',
            ],
            'email' => [
                'required',
                'max:255',
                'email',
                'unique:users',
            ],
            'password' => [
                'required',
                'min:8',
                'confirmed',
            ],
			'password_confirmation' => [
                'required',
                'min:8',
            ],
            'role' => [
                'required',
                'string',
                'in:user',
            ],
        ];

        return $rules;
    }


    /**
    * Get custom attributes for validator errors.
    *
    * @return array
    */
    public function attributes()
    {
        $attributes = [
            'name' => 'Nama',
			'email' => 'Email',
			'password' => 'Password',
			'password_confirmation' => 'Konfirmasi Password',
        ];

        return $attributes;
    }

	/**
     * Handle a failed validation attempt.
     *
     * @param  \Illuminate\Contracts\Validation\Validator  $validator
     * @return void
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    protected function failedValidation(Validator $validator)
    {
        dd($validator->validate());
    }

}
