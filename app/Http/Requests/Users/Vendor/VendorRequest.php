<?php

namespace App\Http\Requests\Users\Vendor;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class VendorRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.s
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'name' => [
                'required',
                'max:255',
            ],
            'email' => [
                'required',
                'max:255',
                'unique:users',
            ],
            'password' => [
                'required',
                'min:6',
            ],
			'role' => [
                'required',
                'in:vendor',
            ],
			'company_name' => [
                'required',
                'max:255',
            ],
			'leader_name' => [
                'required',
                'max:255',
            ],
			'address' => [
                'required',
                'max:255',
            ],
			'phone' => [
                'nullable',
                'max:20',
            ],
			'mobile' => [
                'nullable',
                'max:20',
            ],
			'lat' => [
                'required',
                'numeric',
            ],
			'lng' => [
                'required',
                'numeric',
            ],
        ];

        if ($this->getMethod() == 'PUT') {
            $rules['email'] = [
                'required',
                Rule::unique('users')->ignore($this->vendor),
            ];

			$rules['role'] = [
                'nullable',
            ];
			
			$rules['is_active'] = [
                'required',
				'numeric',
				'in:0,1,2',
            ];

            $rules['password'] = [
                'nullable',
            ];
        }

        return $rules;
    }

    /**
    * Get custom attributes for validator errors.
    *
    * @return array
    */
    public function attributes()
    {
        $attributes = [
            'name' => 'Name',
            'email' => 'Email',
            'is_active' => 'Status',
            'password' => 'Password',
            'company_name' => 'Company name',
            'leader_name' => 'Leader name',
            'address' => 'Address',
            'phone' => 'Phone',
            'mobile' => 'Mobile',
            'mobile' => 'Mobile',
			'lat' => 'Latitude',
            'lng' => 'Longitude',
        ];

        return $attributes;
    }
}
