-- MariaDB dump 10.19  Distrib 10.7.3-MariaDB, for Linux (x86_64)
--
-- Host: localhost    Database: laravel9_starterkit
-- ------------------------------------------------------
-- Server version	10.7.3-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categories` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `desc` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT 1,
  `is_delete` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `categories_name_unique` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categories`
--

LOCK TABLES `categories` WRITE;
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
INSERT INTO `categories` VALUES
(1,'Kertas','Kategori Semua Kertas',1,0,'2022-05-16 10:15:03','2022-05-16 10:15:03'),
(2,'Logam','Kategori Semua Logam',1,0,'2022-05-16 10:15:03','2022-05-16 10:15:03'),
(3,'Plastik','Kategori Semua Plastik',1,0,'2022-05-16 10:15:03','2022-05-16 10:15:03');
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `failed_jobs`
--

DROP TABLE IF EXISTS `failed_jobs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `failed_jobs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`),
  UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `failed_jobs`
--

LOCK TABLES `failed_jobs` WRITE;
/*!40000 ALTER TABLE `failed_jobs` DISABLE KEYS */;
/*!40000 ALTER TABLE `failed_jobs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=185 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES
(171,'2014_10_12_000000_create_users_table',1),
(172,'2014_10_12_100000_create_password_resets_table',1),
(173,'2014_10_12_200000_add_two_factor_columns_to_users_table',1),
(174,'2019_08_19_000000_create_failed_jobs_table',1),
(175,'2019_12_14_000001_create_personal_access_tokens_table',1),
(176,'2020_05_21_100000_create_teams_table',1),
(177,'2020_05_21_200000_create_team_user_table',1),
(178,'2020_05_21_300000_create_team_invitations_table',1),
(179,'2022_04_15_112615_create_sessions_table',1),
(180,'2022_04_23_124318_create_categories_table',1),
(181,'2022_04_24_094931_create_user_details_table',1),
(182,'2022_05_14_141856_create_products_table',1),
(183,'2022_05_16_165158_create_transactions_table',1),
(184,'2022_05_16_165206_create_transaction_details_table',1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `personal_access_tokens`
--

DROP TABLE IF EXISTS `personal_access_tokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `personal_access_tokens` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `tokenable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint(20) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `personal_access_tokens`
--

LOCK TABLES `personal_access_tokens` WRITE;
/*!40000 ALTER TABLE `personal_access_tokens` DISABLE KEYS */;
/*!40000 ALTER TABLE `personal_access_tokens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `products`
--

DROP TABLE IF EXISTS `products`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `products` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `vendor_id` bigint(20) unsigned NOT NULL,
  `category_id` bigint(20) unsigned NOT NULL,
  `name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `desc` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` int(11) NOT NULL,
  `discount` smallint(6) NOT NULL,
  `stock` smallint(6) NOT NULL DEFAULT 0,
  `is_active` tinyint(1) NOT NULL DEFAULT 1,
  `is_delete` tinyint(1) NOT NULL DEFAULT 0,
  `logo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `products_vendor_id_foreign` (`vendor_id`),
  KEY `products_category_id_foreign` (`category_id`),
  CONSTRAINT `products_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE,
  CONSTRAINT `products_vendor_id_foreign` FOREIGN KEY (`vendor_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `products`
--

LOCK TABLES `products` WRITE;
/*!40000 ALTER TABLE `products` DISABLE KEYS */;
INSERT INTO `products` VALUES
(1,3,2,'Aluminium','Aluminium Bekas',15000,0,0,1,0,NULL,'2022-05-16 10:16:12','2022-05-16 10:16:12'),
(2,3,3,'Botol Air Mineral Bening','Botol Air Mineral Bening',2000,0,0,1,0,NULL,'2022-05-16 10:16:54','2022-05-16 10:16:54'),
(3,3,1,'Kertas Koran','Kertas Koran',2500,0,0,1,0,NULL,'2022-05-16 10:17:15','2022-05-16 10:17:15');
/*!40000 ALTER TABLE `products` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sessions`
--

DROP TABLE IF EXISTS `sessions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sessions` (
  `id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) unsigned DEFAULT NULL,
  `ip_address` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_agent` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `payload` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_activity` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `sessions_user_id_index` (`user_id`),
  KEY `sessions_last_activity_index` (`last_activity`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sessions`
--

LOCK TABLES `sessions` WRITE;
/*!40000 ALTER TABLE `sessions` DISABLE KEYS */;
INSERT INTO `sessions` VALUES
('b1mTu34MT2XoMOm0xtvbk8twbSCMRcA7xhq5S2p4',3,'127.0.0.1','Mozilla/5.0 (X11; Linux x86_64; rv:100.0) Gecko/20100101 Firefox/100.0','YTo1OntzOjY6Il90b2tlbiI7czo0MDoiWUtCWlpxblgyUzNsdjBXNFJNcGx1Mnp0aE55a05XVTFQdE9EdnZiUSI7czozOiJ1cmwiO2E6MDp7fXM6NjoiX2ZsYXNoIjthOjI6e3M6Mzoib2xkIjthOjA6e31zOjM6Im5ldyI7YTowOnt9fXM6NTA6ImxvZ2luX3dlYl81OWJhMzZhZGRjMmIyZjk0MDE1ODBmMDE0YzdmNThlYTRlMzA5ODlkIjtpOjM7czoyMToicGFzc3dvcmRfaGFzaF9zYW5jdHVtIjtzOjYwOiIkMnkkMTAkQXNEUDFRZW56ZmJrcXVlNWFxbDRULm5IajFXR3l5YWowbXNCZVNOaWlZdC5NYmlJblFQSGEiO30=',1652696445);
/*!40000 ALTER TABLE `sessions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `team_invitations`
--

DROP TABLE IF EXISTS `team_invitations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `team_invitations` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `team_id` bigint(20) unsigned NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `team_invitations_team_id_email_unique` (`team_id`,`email`),
  CONSTRAINT `team_invitations_team_id_foreign` FOREIGN KEY (`team_id`) REFERENCES `teams` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `team_invitations`
--

LOCK TABLES `team_invitations` WRITE;
/*!40000 ALTER TABLE `team_invitations` DISABLE KEYS */;
/*!40000 ALTER TABLE `team_invitations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `team_user`
--

DROP TABLE IF EXISTS `team_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `team_user` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `team_id` bigint(20) unsigned NOT NULL,
  `user_id` bigint(20) unsigned NOT NULL,
  `role` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `team_user_team_id_user_id_unique` (`team_id`,`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `team_user`
--

LOCK TABLES `team_user` WRITE;
/*!40000 ALTER TABLE `team_user` DISABLE KEYS */;
/*!40000 ALTER TABLE `team_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `teams`
--

DROP TABLE IF EXISTS `teams`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `teams` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_team` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `teams_user_id_index` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `teams`
--

LOCK TABLES `teams` WRITE;
/*!40000 ALTER TABLE `teams` DISABLE KEYS */;
/*!40000 ALTER TABLE `teams` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `transaction_details`
--

DROP TABLE IF EXISTS `transaction_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `transaction_details` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_id` bigint(20) unsigned NOT NULL,
  `product_name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `qty` int(11) NOT NULL,
  `price` bigint(20) NOT NULL,
  `total` double(15,2) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `transaction_details`
--

LOCK TABLES `transaction_details` WRITE;
/*!40000 ALTER TABLE `transaction_details` DISABLE KEYS */;
/*!40000 ALTER TABLE `transaction_details` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `transactions`
--

DROP TABLE IF EXISTS `transactions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `transactions` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `vendor_id` bigint(20) unsigned NOT NULL,
  `member_id` bigint(20) unsigned NOT NULL,
  `transaction_number` bigint(20) NOT NULL,
  `ref_number` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `total` double(15,2) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `trans_unique` (`vendor_id`,`transaction_number`),
  KEY `transactions_member_id_foreign` (`member_id`),
  CONSTRAINT `transactions_member_id_foreign` FOREIGN KEY (`member_id`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  CONSTRAINT `transactions_vendor_id_foreign` FOREIGN KEY (`vendor_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `transactions`
--

LOCK TABLES `transactions` WRITE;
/*!40000 ALTER TABLE `transactions` DISABLE KEYS */;
/*!40000 ALTER TABLE `transactions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_details`
--

DROP TABLE IF EXISTS `user_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_details` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) unsigned NOT NULL,
  `company_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `leader_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mobile` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lat` double DEFAULT 0,
  `lng` double DEFAULT 0,
  `logo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_details_user_id_foreign` (`user_id`),
  CONSTRAINT `user_details_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_details`
--

LOCK TABLES `user_details` WRITE;
/*!40000 ALTER TABLE `user_details` DISABLE KEYS */;
INSERT INTO `user_details` VALUES
('22122602-5106-4172-84ee-f1717963617b',13,'Bank Sampah Mekar Melati','-','Gg. Slamet, Notoprajan, Ngampilan, Kota Yogyakarta, Daerah Istimewa Yogyakarta 55262','','',-7.802013753855763,110.35505498819064,NULL,'2022-05-16 10:15:03','2022-05-16 10:15:03'),
('24ed04e5-cdc2-4f85-8eb4-36919becc938',8,'Bank Sampah Pelangi','-','Jl. H. Agus Salim No.74, Notoprajan, Ngampilan, Kota Yogyakarta, Daerah Istimewa Yogyakarta 55262','','',-7.80365865056546,110.35824223401659,NULL,'2022-05-16 10:15:03','2022-05-16 10:15:03'),
('28ee1470-9431-4cb1-b880-61829388b016',4,'Bank Sampah Asri','Ahmad Dhani','Gambiran Baru UH/5 53 RT 45 RW 008 Kel Pandeyan Kec Umbulharjo Yogyakarta','0274 112233','0987654321',-7.81730406708698,110.39343651080071,NULL,'2022-05-16 10:15:03','2022-05-16 10:15:03'),
('292b7ae0-ffec-4656-8e44-8abe538e93a4',17,'Bank Sampah Sorwo','-','Jl. Masjid Khusnul Khotimah, Bumijo, Kec. Jetis, Kota Yogyakarta, Daerah Istimewa Yogyakarta 55231','','',-7.783669031032129,110.36269925750543,NULL,'2022-05-16 10:15:03','2022-05-16 10:15:03'),
('3a4a925b-874e-423e-9692-a956b4d8bacd',15,'Bank Sampah BERSERI','-','Jl. Cik Di Tiro No.5, Terban, Kec. Gondokusuman, Kota Yogyakarta, Daerah Istimewa Yogyakarta 55223','','',-7.78004922718948,110.37453441517673,NULL,'2022-05-16 10:15:03','2022-05-16 10:15:03'),
('484b7419-a77a-46f6-826b-1a0656b83ec4',11,'Bank Sampah Pilah Pilih Mandiri','-','Jl. KH Wahid Hasyim No.8, Notoprajan, Ngampilan, Kota Yogyakarta, Daerah Istimewa Yogyakarta 55262','','',-7.804643861558739,110.35614757612429,NULL,'2022-05-16 10:15:03','2022-05-16 10:15:03'),
('67ac7aad-beb4-4f24-aca6-d2b6fe79fb71',3,'Bank Sampah Kalasan','Zohan','Jl. Raya Yogya - Solo, Suryatmajan, Danurejan, Daerah Istimewa Yogyakarta','0274 123456','0987654321',-7.766818518155677,110.47264512063761,NULL,'2022-05-16 10:15:03','2022-05-16 10:15:03'),
('73b3a6bf-42ab-498a-af63-e13c46827710',10,'Bank Sampah Surolaras','-','Jl. Suronatan No.Blok NG-2/51, Notoprajan, Ngampilan, Kota Yogyakarta, Daerah Istimewa Yogyakarta 55262','','',-7.801986489888218,110.35953788819063,NULL,'2022-05-16 10:15:03','2022-05-16 10:15:03'),
('8b02a5ad-8cbd-4df2-93d6-86889e91cd19',21,'Bank Sampah Barokah Daleman Prenggan','-','Gg. Gemah Ripah, Purbayan, Kec. Kotagede, Kota Yogyakarta, Daerah Istimewa Yogyakarta 55173','','',-7.831473113692687,110.40009185750543,NULL,'2022-05-16 10:15:03','2022-05-16 10:15:03'),
('993f97b4-7fca-4252-9783-b8cf4c2e074a',6,'Bank Sampah Damai Bersatu','-','Jl. Brigjen Katamso No.1188 B, Prawirodirjan, Kec. Gondomanan, Kota Yogyakarta, Daerah Istimewa Yogyakarta 55121','','',-7.805610702939828,110.37018020409892,NULL,'2022-05-16 10:15:03','2022-05-16 10:15:03'),
('a2f6edf9-6586-4194-b7f1-0c1dba0caa73',14,'Bank Sampah Suryo Resik','-','Mj 2/822, RT.44/RW.13, Suryodiningratan, Kec. Mantrijeron, Kota Yogyakarta, Daerah Istimewa Yogyakarta 55141','','',-7.818638949091695,110.35915183051935,NULL,'2022-05-16 10:15:03','2022-05-16 10:15:03'),
('bb98c802-ee60-4618-b528-80783d6a7d09',19,'Banks Sampah Reresik','-','Jl. Prawirotaman No.21 B, Brontokusuman, Kec. Mergangsan, Kota Yogyakarta, Daerah Istimewa Yogyakarta 55153','','',-7.8185852326892915,110.37070159983413,NULL,'2022-05-16 10:15:03','2022-05-16 10:15:03'),
('be705c3c-ce2c-4314-b6fc-5a56c0083952',5,'Bank Sampah Blazent','-','Jl. Taman Siswa No.7, Gunungketur, Pakualaman, Kota Yogyakarta, Daerah Istimewa Yogyakarta 55151','','',-7.80218227529431,110.37777703121586,NULL,'2022-05-16 10:15:03','2022-05-16 10:15:03'),
('c27b79e4-abaf-439e-aaa7-97ab7c942031',23,'Bank Sampah Mondoroko RW 7','-','Jl. Mondorakan No.27, Prenggan, Kec. Kotagede, Kota Yogyakarta, Daerah Istimewa Yogyakarta 55172','','+6281804320636',-7.827177040099077,110.39632664216282,NULL,'2022-05-16 10:15:03','2022-05-16 10:15:03'),
('c7392bd3-d079-435a-9836-2d44473ccd81',22,'Bank Sampah Serangan Kudu Resik','-','Serangan NG II 216, RT.14/RW.02, Notoprajan, Ngampilan, Kota Yogyakarta, Daerah Istimewa Yogyakarta 55262','','',-7.803713970523588,110.35589838449151,NULL,'2022-05-16 10:15:03','2022-05-16 10:15:03'),
('d0cc251b-8369-42e8-965d-9d6f5e1cafd3',12,'Bank Sampah Resik lan Pikoleh','-','Unnamed Road, Suryodiningratan, Kec. Mantrijeron, Kota Yogyakarta, Daerah Istimewa Yogyakarta 55141','','',-7.814265585910109,110.36216083051934,NULL,'2022-05-16 10:15:03','2022-05-16 10:15:03'),
('d518e3be-153b-489a-a5fd-554951832b7a',16,'Bank Sampah Sido Resik','-','Jl. Mangkuyudan No.18, Mantrijeron, Kec. Mantrijeron, Kota Yogyakarta, Daerah Istimewa Yogyakarta 55143','','',-7.82070997717165,110.36323485750546,NULL,'2022-05-16 10:15:03','2022-05-16 10:15:03'),
('e6b5dbdc-9b92-4739-973f-bb08d8575c53',9,'Bank Sampah MAKMUR Kotabaru','-','Jl. Atmosukarto No.9, RT.11/RW.03, Kotabaru, Kec. Gondokusuman, Kota Yogyakarta, Daerah Istimewa Yogyakarta 55224','','',-7.78799823505202,110.37626642682022,NULL,'2022-05-16 10:15:03','2022-05-16 10:15:03'),
('eba0ecaf-b6f8-4571-9e25-b0e7fc9a4a58',20,'Bank Sampah Pokoke Resik','-','Gg. Darusalam No.602, Notoprajan, Ngampilan, Kota Yogyakarta, Daerah Istimewa Yogyakarta 55262','','',-7.801672934255056,110.35734745750544,NULL,'2022-05-16 10:15:03','2022-05-16 10:15:03'),
('f04de708-3dae-456a-ad80-ff8f261c06a0',18,'Bank Sampah Pandan Wangi Demangan','-','Jl. Nangka No.584, Demangan, Kec. Gondokusuman, Kota Yogyakarta, Daerah Istimewa Yogyakarta 55221','','',-7.785045271096105,110.38970765750544,NULL,'2022-05-16 10:15:03','2022-05-16 10:15:03'),
('fb1b6a84-cc09-4b75-8390-e39eacedc89b',7,'Bank Sampah Tresno Tuhutentrem','-','Jl. Sorosutan No.33, RW.5, Sorosutan, Kec. Umbulharjo, Kota Yogyakarta, Daerah Istimewa Yogyakarta 55162','','+6281904260064',-7.8203844579403095,110.37873197622105,NULL,'2022-05-16 10:15:03','2022-05-16 10:15:03');
/*!40000 ALTER TABLE `user_details` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `two_factor_secret` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `two_factor_recovery_codes` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `two_factor_confirmed_at` timestamp NULL DEFAULT NULL,
  `role` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'user' COMMENT 'user, vendor, admin, super-admin',
  `is_active` smallint(6) NOT NULL DEFAULT 1 COMMENT '0 = inactive, 1 = active, 2 = blocked',
  `balance` double NOT NULL DEFAULT 0,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `current_team_id` bigint(20) unsigned DEFAULT NULL,
  `profile_photo_path` varchar(2048) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=124 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES
(1,'Super Admin','super@gmail.com',NULL,'$2y$10$lZ4GCajaDE6zwv39WsrfyOD3SWAGJXrYQEt9j1h8lqB/2kjsfQCV.',NULL,NULL,NULL,'super-admin',1,0,NULL,NULL,NULL,'2022-05-16 10:15:02','2022-05-16 10:15:02'),
(2,'Admin','admin@gmail.com',NULL,'$2y$10$jeAuxyamPTF7q0p1x4uPvOIdJQpBccJRo9pkzSkwMqxU8LtmKYkUi',NULL,NULL,NULL,'admin',1,0,NULL,NULL,NULL,'2022-05-16 10:15:02','2022-05-16 10:15:02'),
(3,'Bank Sampah Kalasan','joglostudio@gmail.com',NULL,'$2y$10$AsDP1Qenzfbkque5aql4T.nHj1WGyyaj0msBeSNiiYt.MbiInQPHa',NULL,NULL,NULL,'vendor',1,0,NULL,NULL,NULL,'2022-05-16 10:15:03','2022-05-16 10:15:03'),
(4,'Bank Sampah Asri','banksampahasri@gmail.com',NULL,'$2y$10$3HebYGqiPul09WPgtYPEVeIHfB5Vgq9VPTPvIcNwa3HglJw5l85Mi',NULL,NULL,NULL,'vendor',1,0,NULL,NULL,NULL,'2022-05-16 10:15:03','2022-05-16 10:15:03'),
(5,'Bank Sampah Blazent','template1@example.com',NULL,'$2y$10$9xczf1yAxmJ9P2CsOWNCI.GoBUc4tGQEV7tZbZz3U/glxJzN31mH6',NULL,NULL,NULL,'vendor',1,0,NULL,NULL,NULL,'2022-05-16 10:15:03','2022-05-16 10:15:03'),
(6,'Bank Sampah Damai Bersatu','template2@example.com',NULL,'$2y$10$3Mmhg09XD6hvLyDpDLY.o.rUSIITDEy1G6G6Lnupig5W7spkStDqS',NULL,NULL,NULL,'vendor',1,0,NULL,NULL,NULL,'2022-05-16 10:15:03','2022-05-16 10:15:03'),
(7,'Bank Sampah Tresno Tuhutentrem','template3@example.com',NULL,'$2y$10$PCL78VWqkde4zOx8tXUwZ.tUhLldtJ5j2wDBspD8VXdu9ybP.RXU2',NULL,NULL,NULL,'vendor',1,0,NULL,NULL,NULL,'2022-05-16 10:15:03','2022-05-16 10:15:03'),
(8,'Bank Sampah Pelangi','template4@example.com',NULL,'$2y$10$KG7Et9l9487DGB/TfDUGSe7N1ASBWMGcbiDTj9ZCVYRYQ6rOCs14u',NULL,NULL,NULL,'vendor',1,0,NULL,NULL,NULL,'2022-05-16 10:15:03','2022-05-16 10:15:03'),
(9,'Bank Sampah MAKMUR Kotabaru','template5@example.com',NULL,'$2y$10$foP8ELyuqr608.XYQ.ze5egly6QF3V3l6nENYXf3uVbDSee2exF4u',NULL,NULL,NULL,'vendor',1,0,NULL,NULL,NULL,'2022-05-16 10:15:03','2022-05-16 10:15:03'),
(10,'Bank Sampah Surolaras','template6@example.com',NULL,'$2y$10$43uLuNuXSUl1y8ATTm.oqeGcfhyuSFMEcsBUOGuZZy5JXxZMzu6Pi',NULL,NULL,NULL,'vendor',1,0,NULL,NULL,NULL,'2022-05-16 10:15:03','2022-05-16 10:15:03'),
(11,'Bank Sampah Pilah Pilih Mandiri','template7@example.com',NULL,'$2y$10$NOem/FscU9gCUiQ3vM0c3.o0GJK3kwvbVsSLyFtHpQdPhphXUMysW',NULL,NULL,NULL,'vendor',1,0,NULL,NULL,NULL,'2022-05-16 10:15:03','2022-05-16 10:15:03'),
(12,'Bank Sampah Resik lan Pikoleh','template8@example.com',NULL,'$2y$10$/tDW1v8tW3iu8.HCx1WNdOtUGuStJXFDF6HZQggDTCLOUJ9gPyJHW',NULL,NULL,NULL,'vendor',1,0,NULL,NULL,NULL,'2022-05-16 10:15:03','2022-05-16 10:15:03'),
(13,'Bank Sampah Mekar Melati','template9@example.com',NULL,'$2y$10$p9EWjzL/mdLkpgkqSxirUeaRThXjxSdZHyzzYMbNlWEr3nBB1Kjfq',NULL,NULL,NULL,'vendor',1,0,NULL,NULL,NULL,'2022-05-16 10:15:03','2022-05-16 10:15:03'),
(14,'Bank Sampah Suryo Resik','template10@example.com',NULL,'$2y$10$H7RxobpoOzrSfGIhvnoqyOWLfdWmhPpuEmKuby.jtUoywE8UNAApK',NULL,NULL,NULL,'vendor',1,0,NULL,NULL,NULL,'2022-05-16 10:15:03','2022-05-16 10:15:03'),
(15,'Bank Sampah BERSERI','template11@example.com',NULL,'$2y$10$JxjPBgveYfbSovwoh6uI6OdtQOiZBrxlvvXittc7QYvEQmTYJYGde',NULL,NULL,NULL,'vendor',1,0,NULL,NULL,NULL,'2022-05-16 10:15:03','2022-05-16 10:15:03'),
(16,'Bank Sampah Sido Resik','template12@example.com',NULL,'$2y$10$GOj2eq.jU9bEOph1utUbx..lcIf2hmtqW9kz5DEkn.KCf.mnpexVu',NULL,NULL,NULL,'vendor',1,0,NULL,NULL,NULL,'2022-05-16 10:15:03','2022-05-16 10:15:03'),
(17,'Bank Sampah Sorwo','template14@example.com',NULL,'$2y$10$xvLQ40MkbTrBcPvl04mlWeEAiUT.S7nO7vy/ql4FW2Vtc/5piP60e',NULL,NULL,NULL,'vendor',1,0,NULL,NULL,NULL,'2022-05-16 10:15:03','2022-05-16 10:15:03'),
(18,'Bank Sampah Pandan Wangi Demangan','template15@example.com',NULL,'$2y$10$fpJNHdM./f945dtuq/HBSeTLIe0/DFbdoKgeA.1GbjXvoQUE.Mylq',NULL,NULL,NULL,'vendor',1,0,NULL,NULL,NULL,'2022-05-16 10:15:03','2022-05-16 10:15:03'),
(19,'Banks Sampah Reresik','template16@example.com',NULL,'$2y$10$s8GIWNTIOWxx1Ik8HZ4wYe.l19YqHYNuODuNuuNC.rVhzm5XE79Fe',NULL,NULL,NULL,'vendor',1,0,NULL,NULL,NULL,'2022-05-16 10:15:03','2022-05-16 10:15:03'),
(20,'Bank Sampah Pokoke Resik','template17@example.com',NULL,'$2y$10$D504eiJ9mPLca1HUEw5dXuz6G40k2yVoEZ/9bMLJISvDLS.LssHru',NULL,NULL,NULL,'vendor',1,0,NULL,NULL,NULL,'2022-05-16 10:15:03','2022-05-16 10:15:03'),
(21,'Bank Sampah Barokah Daleman Prenggan','template18@example.com',NULL,'$2y$10$6CX4IQ.DTNzV3yUv88U2u.L.wNJ3r7B.3Eb28kpYUOapPbyGDd8F.',NULL,NULL,NULL,'vendor',1,0,NULL,NULL,NULL,'2022-05-16 10:15:03','2022-05-16 10:15:03'),
(22,'Bank Sampah Serangan Kudu Resik','template19@example.com',NULL,'$2y$10$nHSS5aO/SY3tii6XKRmB8.5jkNv5ovi4LPDC9Nh6M2jxZMVW3QO5u',NULL,NULL,NULL,'vendor',1,0,NULL,NULL,NULL,'2022-05-16 10:15:03','2022-05-16 10:15:03'),
(23,'Bank Sampah Mondoroko RW 7','template20@example.com',NULL,'$2y$10$Lq7EhhLMd/Di2N.uu69qeO7TjeeqMzRQFuzHtCbCqrwft5MynaqCu',NULL,NULL,NULL,'vendor',1,0,NULL,NULL,NULL,'2022-05-16 10:15:03','2022-05-16 10:15:03'),
(24,'Crystal Douglas','walker.allie@example.com','2022-05-16 10:15:03','$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi',NULL,NULL,NULL,'user',1,0,'aKjr87TPbp',NULL,NULL,'2022-05-16 10:15:03','2022-05-16 10:15:03'),
(25,'Spencer Buckridge IV','walsh.brown@example.org','2022-05-16 10:15:03','$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi',NULL,NULL,NULL,'user',1,0,'XR0fQPZ2jB',NULL,NULL,'2022-05-16 10:15:04','2022-05-16 10:15:04'),
(26,'Jovan Murray','rippin.dusty@example.net','2022-05-16 10:15:03','$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi',NULL,NULL,NULL,'user',1,0,'ww0I79ETFV',NULL,NULL,'2022-05-16 10:15:04','2022-05-16 10:15:04'),
(27,'Kiana Padberg','fern.kertzmann@example.org','2022-05-16 10:15:03','$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi',NULL,NULL,NULL,'user',1,0,'NbjB6FVkui',NULL,NULL,'2022-05-16 10:15:04','2022-05-16 10:15:04'),
(28,'Mrs. Maurine Abbott DVM','jess61@example.com','2022-05-16 10:15:03','$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi',NULL,NULL,NULL,'user',1,0,'Lc3azcJwL2',NULL,NULL,'2022-05-16 10:15:04','2022-05-16 10:15:04'),
(29,'Luella Kunde','cassin.wilfred@example.net','2022-05-16 10:15:03','$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi',NULL,NULL,NULL,'user',1,0,'SyNGuHFCCC',NULL,NULL,'2022-05-16 10:15:04','2022-05-16 10:15:04'),
(30,'Monroe Sipes','zakary.stehr@example.org','2022-05-16 10:15:03','$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi',NULL,NULL,NULL,'user',1,0,'LGXZURHMZi',NULL,NULL,'2022-05-16 10:15:04','2022-05-16 10:15:04'),
(31,'Prof. Myles Rippin','annabell93@example.net','2022-05-16 10:15:03','$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi',NULL,NULL,NULL,'user',1,0,'LvQxeoHgQf',NULL,NULL,'2022-05-16 10:15:04','2022-05-16 10:15:04'),
(32,'Opal Jerde PhD','cyril21@example.org','2022-05-16 10:15:03','$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi',NULL,NULL,NULL,'user',1,0,'mTpAlULf1g',NULL,NULL,'2022-05-16 10:15:04','2022-05-16 10:15:04'),
(33,'Mireya Zboncak','iherman@example.org','2022-05-16 10:15:03','$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi',NULL,NULL,NULL,'user',1,0,'JQRQqkOiGw',NULL,NULL,'2022-05-16 10:15:04','2022-05-16 10:15:04'),
(34,'Shayne Hoeger','hschiller@example.org','2022-05-16 10:15:03','$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi',NULL,NULL,NULL,'user',1,0,'UV4EUyOzMg',NULL,NULL,'2022-05-16 10:15:04','2022-05-16 10:15:04'),
(35,'Tyree Sipes','xkessler@example.org','2022-05-16 10:15:03','$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi',NULL,NULL,NULL,'user',1,0,'OoKCsMnVst',NULL,NULL,'2022-05-16 10:15:04','2022-05-16 10:15:04'),
(36,'Prof. Marlin Waelchi V','ugutkowski@example.com','2022-05-16 10:15:03','$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi',NULL,NULL,NULL,'user',1,0,'daU7DQ4ipk',NULL,NULL,'2022-05-16 10:15:04','2022-05-16 10:15:04'),
(37,'Eula Walsh','sammie.von@example.com','2022-05-16 10:15:03','$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi',NULL,NULL,NULL,'user',1,0,'uBAoyBolaX',NULL,NULL,'2022-05-16 10:15:04','2022-05-16 10:15:04'),
(38,'Ruthe Gusikowski IV','brannon74@example.com','2022-05-16 10:15:03','$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi',NULL,NULL,NULL,'user',1,0,'meDZ8odANM',NULL,NULL,'2022-05-16 10:15:04','2022-05-16 10:15:04'),
(39,'Nakia Huels','brooke.johnson@example.net','2022-05-16 10:15:03','$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi',NULL,NULL,NULL,'user',1,0,'FoZogSzvUr',NULL,NULL,'2022-05-16 10:15:04','2022-05-16 10:15:04'),
(40,'Javonte Wunsch','nader.gudrun@example.com','2022-05-16 10:15:03','$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi',NULL,NULL,NULL,'user',1,0,'4B8imfmy9f',NULL,NULL,'2022-05-16 10:15:04','2022-05-16 10:15:04'),
(41,'Genesis Fay DVM','bernhard.abdiel@example.org','2022-05-16 10:15:03','$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi',NULL,NULL,NULL,'user',1,0,'HrMfRSxiYz',NULL,NULL,'2022-05-16 10:15:04','2022-05-16 10:15:04'),
(42,'Yasmeen Gleichner','kenya67@example.org','2022-05-16 10:15:03','$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi',NULL,NULL,NULL,'user',1,0,'IpYkFpc8qw',NULL,NULL,'2022-05-16 10:15:04','2022-05-16 10:15:04'),
(43,'Jennyfer Schuster','lang.mervin@example.com','2022-05-16 10:15:03','$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi',NULL,NULL,NULL,'user',1,0,'NbOT8DLNpx',NULL,NULL,'2022-05-16 10:15:04','2022-05-16 10:15:04'),
(44,'Luigi Adams','sawayn.bria@example.org','2022-05-16 10:15:03','$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi',NULL,NULL,NULL,'user',1,0,'8x3daBXuQA',NULL,NULL,'2022-05-16 10:15:04','2022-05-16 10:15:04'),
(45,'Jazlyn Kautzer IV','gregoria74@example.net','2022-05-16 10:15:03','$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi',NULL,NULL,NULL,'user',1,0,'denWpzKm53',NULL,NULL,'2022-05-16 10:15:04','2022-05-16 10:15:04'),
(46,'Bo Mertz','sipes.jevon@example.net','2022-05-16 10:15:03','$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi',NULL,NULL,NULL,'user',1,0,'zhQVopnWJy',NULL,NULL,'2022-05-16 10:15:04','2022-05-16 10:15:04'),
(47,'Travis Koch','brigitte20@example.net','2022-05-16 10:15:03','$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi',NULL,NULL,NULL,'user',1,0,'W0b5i0g98E',NULL,NULL,'2022-05-16 10:15:04','2022-05-16 10:15:04'),
(48,'Darien Wunsch','rice.colton@example.net','2022-05-16 10:15:03','$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi',NULL,NULL,NULL,'user',1,0,'yNmxJM06yd',NULL,NULL,'2022-05-16 10:15:04','2022-05-16 10:15:04'),
(49,'Hilbert Torp','pansy37@example.net','2022-05-16 10:15:03','$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi',NULL,NULL,NULL,'user',1,0,'KLUr6QSNL5',NULL,NULL,'2022-05-16 10:15:04','2022-05-16 10:15:04'),
(50,'Dr. Euna Stark V','corkery.vida@example.net','2022-05-16 10:15:03','$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi',NULL,NULL,NULL,'user',1,0,'YKC6B6FPgl',NULL,NULL,'2022-05-16 10:15:04','2022-05-16 10:15:04'),
(51,'Juston Zboncak','katelyn82@example.net','2022-05-16 10:15:03','$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi',NULL,NULL,NULL,'user',1,0,'tImaYQC1AY',NULL,NULL,'2022-05-16 10:15:04','2022-05-16 10:15:04'),
(52,'Don Ernser','ahmed25@example.com','2022-05-16 10:15:03','$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi',NULL,NULL,NULL,'user',1,0,'0jqBWHdYLI',NULL,NULL,'2022-05-16 10:15:04','2022-05-16 10:15:04'),
(53,'Lilla Bergstrom PhD','jones.emelia@example.com','2022-05-16 10:15:03','$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi',NULL,NULL,NULL,'user',1,0,'o8D5FsEyEc',NULL,NULL,'2022-05-16 10:15:04','2022-05-16 10:15:04'),
(54,'Ms. Marguerite Ward','linnie.cummerata@example.com','2022-05-16 10:15:03','$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi',NULL,NULL,NULL,'user',1,0,'X45XkHFwjc',NULL,NULL,'2022-05-16 10:15:04','2022-05-16 10:15:04'),
(55,'Dr. Karelle Abernathy','hermiston.destany@example.com','2022-05-16 10:15:03','$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi',NULL,NULL,NULL,'user',1,0,'INAQ5n5LGI',NULL,NULL,'2022-05-16 10:15:04','2022-05-16 10:15:04'),
(56,'Eddie Crona','hermiston.felipa@example.com','2022-05-16 10:15:03','$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi',NULL,NULL,NULL,'user',1,0,'KkyNYx1MEF',NULL,NULL,'2022-05-16 10:15:04','2022-05-16 10:15:04'),
(57,'Giles Nitzsche','gvandervort@example.com','2022-05-16 10:15:03','$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi',NULL,NULL,NULL,'user',1,0,'OvUCyqllbU',NULL,NULL,'2022-05-16 10:15:04','2022-05-16 10:15:04'),
(58,'Jerel Thiel','kaitlyn.anderson@example.net','2022-05-16 10:15:03','$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi',NULL,NULL,NULL,'user',1,0,'sBHzPYqueu',NULL,NULL,'2022-05-16 10:15:04','2022-05-16 10:15:04'),
(59,'Bette Stokes MD','rcasper@example.org','2022-05-16 10:15:03','$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi',NULL,NULL,NULL,'user',1,0,'E3WAuzLyYP',NULL,NULL,'2022-05-16 10:15:04','2022-05-16 10:15:04'),
(60,'Emmalee Waelchi','hickle.caesar@example.com','2022-05-16 10:15:03','$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi',NULL,NULL,NULL,'user',1,0,'SSJdsUDTfH',NULL,NULL,'2022-05-16 10:15:04','2022-05-16 10:15:04'),
(61,'Dr. Carmella Rosenbaum MD','nicolas.nelson@example.org','2022-05-16 10:15:03','$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi',NULL,NULL,NULL,'user',1,0,'zVrk8My9NW',NULL,NULL,'2022-05-16 10:15:04','2022-05-16 10:15:04'),
(62,'Ms. Ruthe Beier II','snicolas@example.net','2022-05-16 10:15:03','$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi',NULL,NULL,NULL,'user',1,0,'JfzBcqUpgL',NULL,NULL,'2022-05-16 10:15:04','2022-05-16 10:15:04'),
(63,'Omer Stanton','istamm@example.com','2022-05-16 10:15:03','$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi',NULL,NULL,NULL,'user',1,0,'ZsBDJwcmCy',NULL,NULL,'2022-05-16 10:15:04','2022-05-16 10:15:04'),
(64,'Josiah Reynolds','zmaggio@example.com','2022-05-16 10:15:03','$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi',NULL,NULL,NULL,'user',1,0,'eYbVceoY6T',NULL,NULL,'2022-05-16 10:15:04','2022-05-16 10:15:04'),
(65,'Milford Emmerich','bahringer.oleta@example.org','2022-05-16 10:15:03','$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi',NULL,NULL,NULL,'user',1,0,'PguSyp4Wcx',NULL,NULL,'2022-05-16 10:15:04','2022-05-16 10:15:04'),
(66,'Haleigh Krajcik','skiles.brennan@example.net','2022-05-16 10:15:03','$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi',NULL,NULL,NULL,'user',1,0,'EZSc51oT2O',NULL,NULL,'2022-05-16 10:15:04','2022-05-16 10:15:04'),
(67,'Dr. Dave Weimann II','kglover@example.net','2022-05-16 10:15:03','$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi',NULL,NULL,NULL,'user',1,0,'TseR6BeCSO',NULL,NULL,'2022-05-16 10:15:04','2022-05-16 10:15:04'),
(68,'Ezekiel Hickle','luella.welch@example.org','2022-05-16 10:15:03','$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi',NULL,NULL,NULL,'user',1,0,'byz6iSifTe',NULL,NULL,'2022-05-16 10:15:04','2022-05-16 10:15:04'),
(69,'Dr. Orland Farrell','amira.wolff@example.com','2022-05-16 10:15:03','$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi',NULL,NULL,NULL,'user',1,0,'u4N50gTcZp',NULL,NULL,'2022-05-16 10:15:04','2022-05-16 10:15:04'),
(70,'Stewart Gibson','myriam23@example.net','2022-05-16 10:15:03','$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi',NULL,NULL,NULL,'user',1,0,'u5BG4O5pTa',NULL,NULL,'2022-05-16 10:15:04','2022-05-16 10:15:04'),
(71,'Prof. Gustave Runolfsdottir Sr.','wdooley@example.org','2022-05-16 10:15:03','$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi',NULL,NULL,NULL,'user',1,0,'KtIKp1OqXV',NULL,NULL,'2022-05-16 10:15:04','2022-05-16 10:15:04'),
(72,'Keith Corkery','welch.ross@example.net','2022-05-16 10:15:03','$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi',NULL,NULL,NULL,'user',1,0,'dZRzoD0bQg',NULL,NULL,'2022-05-16 10:15:04','2022-05-16 10:15:04'),
(73,'Mrs. Kaitlin Hane I','mcartwright@example.net','2022-05-16 10:15:03','$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi',NULL,NULL,NULL,'user',1,0,'uVK7BCEYIM',NULL,NULL,'2022-05-16 10:15:04','2022-05-16 10:15:04'),
(74,'Titus Thompson','iemard@example.net','2022-05-16 10:15:03','$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi',NULL,NULL,NULL,'user',1,0,'CIb1Fxyv6M',NULL,NULL,'2022-05-16 10:15:04','2022-05-16 10:15:04'),
(75,'Amya Bednar','fokuneva@example.org','2022-05-16 10:15:03','$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi',NULL,NULL,NULL,'user',1,0,'9oFtkDqcmO',NULL,NULL,'2022-05-16 10:15:04','2022-05-16 10:15:04'),
(76,'Terrance Corkery','wiegand.wanda@example.com','2022-05-16 10:15:03','$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi',NULL,NULL,NULL,'user',1,0,'wVCNEktzDm',NULL,NULL,'2022-05-16 10:15:04','2022-05-16 10:15:04'),
(77,'Christine Satterfield','julianne.lockman@example.org','2022-05-16 10:15:03','$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi',NULL,NULL,NULL,'user',1,0,'Xv6LsJUXgG',NULL,NULL,'2022-05-16 10:15:04','2022-05-16 10:15:04'),
(78,'Dr. Sophie Glover','sherwood.murray@example.com','2022-05-16 10:15:03','$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi',NULL,NULL,NULL,'user',1,0,'8SBdj9vESu',NULL,NULL,'2022-05-16 10:15:04','2022-05-16 10:15:04'),
(79,'Ashleigh Armstrong Jr.','eddie28@example.com','2022-05-16 10:15:03','$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi',NULL,NULL,NULL,'user',1,0,'1lJfBaZjmG',NULL,NULL,'2022-05-16 10:15:04','2022-05-16 10:15:04'),
(80,'Enrique Ortiz','lockman.noe@example.net','2022-05-16 10:15:03','$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi',NULL,NULL,NULL,'user',1,0,'dDmzf3nYw5',NULL,NULL,'2022-05-16 10:15:04','2022-05-16 10:15:04'),
(81,'Rafaela Walker','price.reta@example.com','2022-05-16 10:15:03','$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi',NULL,NULL,NULL,'user',1,0,'bGbFxhyAqy',NULL,NULL,'2022-05-16 10:15:04','2022-05-16 10:15:04'),
(82,'Oda Hayes','issac07@example.com','2022-05-16 10:15:03','$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi',NULL,NULL,NULL,'user',1,0,'z8CCYPt8Mw',NULL,NULL,'2022-05-16 10:15:04','2022-05-16 10:15:04'),
(83,'Norwood Waters','crist.vance@example.org','2022-05-16 10:15:03','$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi',NULL,NULL,NULL,'user',1,0,'RbS3cns4ei',NULL,NULL,'2022-05-16 10:15:04','2022-05-16 10:15:04'),
(84,'Rowena Weber','nstiedemann@example.net','2022-05-16 10:15:03','$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi',NULL,NULL,NULL,'user',1,0,'H5WrMoUtJ8',NULL,NULL,'2022-05-16 10:15:04','2022-05-16 10:15:04'),
(85,'Alysha Daniel IV','xweissnat@example.net','2022-05-16 10:15:03','$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi',NULL,NULL,NULL,'user',1,0,'UIiXsI0zBR',NULL,NULL,'2022-05-16 10:15:04','2022-05-16 10:15:04'),
(86,'Dr. Filomena Gorczany III','will.talia@example.com','2022-05-16 10:15:03','$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi',NULL,NULL,NULL,'user',1,0,'gnVmm2pCBZ',NULL,NULL,'2022-05-16 10:15:04','2022-05-16 10:15:04'),
(87,'Kole Lynch','chaya46@example.org','2022-05-16 10:15:03','$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi',NULL,NULL,NULL,'user',1,0,'QaI3495kKe',NULL,NULL,'2022-05-16 10:15:04','2022-05-16 10:15:04'),
(88,'Brando Bogisich','hanna.mueller@example.com','2022-05-16 10:15:03','$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi',NULL,NULL,NULL,'user',1,0,'xyuCc23hYI',NULL,NULL,'2022-05-16 10:15:04','2022-05-16 10:15:04'),
(89,'Abagail Aufderhar','demetris35@example.org','2022-05-16 10:15:03','$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi',NULL,NULL,NULL,'user',1,0,'jUI4xvS4KT',NULL,NULL,'2022-05-16 10:15:04','2022-05-16 10:15:04'),
(90,'Prof. Alena Koepp MD','theodore.reinger@example.com','2022-05-16 10:15:03','$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi',NULL,NULL,NULL,'user',1,0,'Rh8YvS1FvH',NULL,NULL,'2022-05-16 10:15:04','2022-05-16 10:15:04'),
(91,'Juliana Rohan','sreilly@example.net','2022-05-16 10:15:03','$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi',NULL,NULL,NULL,'user',1,0,'DsnbPUi4v7',NULL,NULL,'2022-05-16 10:15:04','2022-05-16 10:15:04'),
(92,'Charles Klocko','egrimes@example.org','2022-05-16 10:15:03','$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi',NULL,NULL,NULL,'user',1,0,'I7jbcHi4AO',NULL,NULL,'2022-05-16 10:15:04','2022-05-16 10:15:04'),
(93,'Thaddeus Braun Sr.','aliya33@example.net','2022-05-16 10:15:03','$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi',NULL,NULL,NULL,'user',1,0,'RTGWjMpjEZ',NULL,NULL,'2022-05-16 10:15:04','2022-05-16 10:15:04'),
(94,'D\'angelo Littel','dessie37@example.com','2022-05-16 10:15:03','$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi',NULL,NULL,NULL,'user',1,0,'F6ufO68DaS',NULL,NULL,'2022-05-16 10:15:04','2022-05-16 10:15:04'),
(95,'Mrs. Edyth Hamill','ywatsica@example.org','2022-05-16 10:15:03','$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi',NULL,NULL,NULL,'user',1,0,'QHEhpOv3KQ',NULL,NULL,'2022-05-16 10:15:04','2022-05-16 10:15:04'),
(96,'Riley Glover V','frenner@example.net','2022-05-16 10:15:03','$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi',NULL,NULL,NULL,'user',1,0,'6qwR6R2hDA',NULL,NULL,'2022-05-16 10:15:04','2022-05-16 10:15:04'),
(97,'Dr. Ismael Walker Jr.','bechtelar.rosario@example.net','2022-05-16 10:15:03','$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi',NULL,NULL,NULL,'user',1,0,'4cZxDcj6Kf',NULL,NULL,'2022-05-16 10:15:04','2022-05-16 10:15:04'),
(98,'Eveline Predovic','thaddeus30@example.com','2022-05-16 10:15:03','$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi',NULL,NULL,NULL,'user',1,0,'1aEv5Dv7uK',NULL,NULL,'2022-05-16 10:15:04','2022-05-16 10:15:04'),
(99,'Yasmine DuBuque III','tillman.francis@example.org','2022-05-16 10:15:03','$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi',NULL,NULL,NULL,'user',1,0,'PBDppcU90k',NULL,NULL,'2022-05-16 10:15:04','2022-05-16 10:15:04'),
(100,'Adela Lesch PhD','ymorissette@example.com','2022-05-16 10:15:03','$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi',NULL,NULL,NULL,'user',1,0,'FKCWUvtHAS',NULL,NULL,'2022-05-16 10:15:04','2022-05-16 10:15:04'),
(101,'Emery Romaguera','bernadette61@example.org','2022-05-16 10:15:03','$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi',NULL,NULL,NULL,'user',1,0,'b0V2vCDdxO',NULL,NULL,'2022-05-16 10:15:04','2022-05-16 10:15:04'),
(102,'Ceasar McKenzie III','willard.feeney@example.net','2022-05-16 10:15:03','$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi',NULL,NULL,NULL,'user',1,0,'TIEGSE50Gu',NULL,NULL,'2022-05-16 10:15:04','2022-05-16 10:15:04'),
(103,'Mrs. Micaela Ullrich','mariana.gleason@example.net','2022-05-16 10:15:03','$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi',NULL,NULL,NULL,'user',1,0,'Mdi2wVae7M',NULL,NULL,'2022-05-16 10:15:04','2022-05-16 10:15:04'),
(104,'Serena Langworth','dax.bergstrom@example.net','2022-05-16 10:15:03','$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi',NULL,NULL,NULL,'user',1,0,'0bdshRTwWn',NULL,NULL,'2022-05-16 10:15:04','2022-05-16 10:15:04'),
(105,'Ms. Hulda Frami V','kkuhn@example.com','2022-05-16 10:15:03','$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi',NULL,NULL,NULL,'user',1,0,'V7dbEjY0oj',NULL,NULL,'2022-05-16 10:15:04','2022-05-16 10:15:04'),
(106,'Tremayne Abbott','ypagac@example.org','2022-05-16 10:15:03','$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi',NULL,NULL,NULL,'user',1,0,'zrUz2Eox4G',NULL,NULL,'2022-05-16 10:15:04','2022-05-16 10:15:04'),
(107,'Berneice Little','nklein@example.net','2022-05-16 10:15:03','$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi',NULL,NULL,NULL,'user',1,0,'IzkHUIcfW6',NULL,NULL,'2022-05-16 10:15:04','2022-05-16 10:15:04'),
(108,'Rosalyn Terry','cecilia17@example.com','2022-05-16 10:15:03','$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi',NULL,NULL,NULL,'user',1,0,'Ixr7P6W2V4',NULL,NULL,'2022-05-16 10:15:04','2022-05-16 10:15:04'),
(109,'Leonardo Schmitt','jerde.minnie@example.net','2022-05-16 10:15:03','$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi',NULL,NULL,NULL,'user',1,0,'HYsFnxCW9q',NULL,NULL,'2022-05-16 10:15:04','2022-05-16 10:15:04'),
(110,'Carolyne Grady','deion54@example.com','2022-05-16 10:15:03','$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi',NULL,NULL,NULL,'user',1,0,'iUFqbt7V1x',NULL,NULL,'2022-05-16 10:15:04','2022-05-16 10:15:04'),
(111,'Lennie Labadie','qmurphy@example.net','2022-05-16 10:15:03','$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi',NULL,NULL,NULL,'user',1,0,'LUt2V4oEJc',NULL,NULL,'2022-05-16 10:15:04','2022-05-16 10:15:04'),
(112,'Ms. Virgie Batz','kuhlman.cornell@example.com','2022-05-16 10:15:03','$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi',NULL,NULL,NULL,'user',1,0,'oC9D3JQ4Pw',NULL,NULL,'2022-05-16 10:15:04','2022-05-16 10:15:04'),
(113,'Stephen Stroman V','bednar.carmela@example.net','2022-05-16 10:15:03','$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi',NULL,NULL,NULL,'user',1,0,'F1OQIwk4lo',NULL,NULL,'2022-05-16 10:15:04','2022-05-16 10:15:04'),
(114,'Mr. Holden Kihn','mcdermott.paul@example.com','2022-05-16 10:15:03','$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi',NULL,NULL,NULL,'user',1,0,'UjHmHhVS5m',NULL,NULL,'2022-05-16 10:15:04','2022-05-16 10:15:04'),
(115,'Mia Schroeder DDS','nikita82@example.com','2022-05-16 10:15:03','$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi',NULL,NULL,NULL,'user',1,0,'HpM16bze85',NULL,NULL,'2022-05-16 10:15:04','2022-05-16 10:15:04'),
(116,'Duncan Feil','elaina82@example.net','2022-05-16 10:15:03','$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi',NULL,NULL,NULL,'user',1,0,'qoynyE7aov',NULL,NULL,'2022-05-16 10:15:04','2022-05-16 10:15:04'),
(117,'Angel Kiehn','rhiannon92@example.net','2022-05-16 10:15:03','$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi',NULL,NULL,NULL,'user',1,0,'RIKaDKbUsI',NULL,NULL,'2022-05-16 10:15:04','2022-05-16 10:15:04'),
(118,'Ms. Earlene Gutkowski','itowne@example.net','2022-05-16 10:15:03','$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi',NULL,NULL,NULL,'user',1,0,'nB2JnbLFvu',NULL,NULL,'2022-05-16 10:15:04','2022-05-16 10:15:04'),
(119,'Baylee Lakin','edwardo.schuster@example.org','2022-05-16 10:15:03','$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi',NULL,NULL,NULL,'user',1,0,'vHxZUEULhS',NULL,NULL,'2022-05-16 10:15:04','2022-05-16 10:15:04'),
(120,'Jordan Ward','kevin.brakus@example.net','2022-05-16 10:15:03','$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi',NULL,NULL,NULL,'user',1,0,'HfCJ0jB4ej',NULL,NULL,'2022-05-16 10:15:04','2022-05-16 10:15:04'),
(121,'Mr. Obie Stamm V','eileen93@example.net','2022-05-16 10:15:03','$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi',NULL,NULL,NULL,'user',1,0,'JFYMK2ORbV',NULL,NULL,'2022-05-16 10:15:04','2022-05-16 10:15:04'),
(122,'Morris Nader','alene49@example.org','2022-05-16 10:15:03','$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi',NULL,NULL,NULL,'user',1,0,'e2p1QjkMSl',NULL,NULL,'2022-05-16 10:15:04','2022-05-16 10:15:04'),
(123,'Bud Boehm','parker.emmy@example.com','2022-05-16 10:15:03','$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi',NULL,NULL,NULL,'user',1,0,'qP0HoLiMzK',NULL,NULL,'2022-05-16 10:15:04','2022-05-16 10:15:04');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-05-16 17:23:13
