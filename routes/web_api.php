<?php

use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\Jetstream\Api\User\UserController;
use App\Http\Controllers\Jetstream\Api\User\ProductController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::middleware(['auth:sanctum', 'is-vendor', 'verified'])->group(function () {
    Route::get('users', UserController::class)->name('web.api.users');
    Route::get('products', ProductController::class)->name('web.api.products');
});