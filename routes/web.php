<?php

use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;

//Dashboard/
use App\Http\Controllers\Jetstream\Dashboard\DashboardController;

//Users
use App\Http\Controllers\Jetstream\User\AdminController;
use App\Http\Controllers\Jetstream\User\UserController;
use App\Http\Controllers\Jetstream\User\VendorController;
use App\Http\Controllers\Jetstream\User\UserDetailController;

//Products
use App\Http\Controllers\Jetstream\Product\CategoryController;
use App\Http\Controllers\Jetstream\Product\ProductController;

use App\Http\Controllers\Jetstream\Transaction\TransactionController;
use App\Http\Controllers\Jetstream\Transaction\WithdrawalController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    //return Inertia::render('Welcome', [
    //    'canLogin' => Route::has('login'),
    //    'canRegister' => Route::has('register'),
    //    'laravelVersion' => Application::VERSION,
    //    'phpVersion' => PHP_VERSION,
    //]);

	return redirect()->route('login');
});

//Route::middleware([
//    'auth:sanctum',
//    config('jetstream.auth_session'),
//    'verified',
//])->group(function () {
//    Route::get('/dashboard', function () {
//        return Inertia::render('Dashboard');
//    })->name('dashboard');
//});

Route::middleware(['auth:sanctum', config('jetstream.auth_session'), 'verified'])->group(function () {
	Route::get('dashboard', DashboardController::class)->name('dashboard');
});

Route::middleware(['auth:sanctum', config('jetstream.auth_session'), 'is-admin', 'verified'])->group(function () {
	Route::resource('users', UserController::class);
	Route::resource('vendors', VendorController::class);
	
	Route::resource('categories', CategoryController::class);
});


Route::middleware(['auth:sanctum', config('jetstream.auth_session'), 'is-vendor', 'verified'])->group(function () {
	Route::group(['prefix' => 'vendor'], function () {
		Route::resource('transactions', TransactionController::class);
		Route::resource('withdrawals', WithdrawalController::class);

		Route::put('user_detail/update', UserDetailController::class)->name('user_detail.update');
		
		Route::resource('products', ProductController::class);
	});
});



Route::middleware(['auth:sanctum', config('jetstream.auth_session'), 'is-super-admin', 'verified'])->group(function () {
    Route::resource('admins', AdminController::class);
});
