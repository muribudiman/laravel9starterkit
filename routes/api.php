<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\Api\V1\Auth\LoginController;
use App\Http\Controllers\Api\V1\Auth\LogoutController;
use App\Http\Controllers\Api\V1\Auth\RegisterController;

/** users */
use App\Http\Controllers\Api\V1\Member\MemberController;
use App\Http\Controllers\Api\V1\Member\VendorController;

use App\Http\Controllers\Api\V1\Product\CategoryController;

/** auth */
use App\Http\Controllers\Api\V1\Auth\ProfileController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::prefix('/v1')->group(function () {

	Route::group(['prefix' => 'auth'], function () {
		Route::post('login', LoginController::class);
		Route::post('register', RegisterController::class);
		
		Route::middleware('auth:sanctum')->group(function () {
			Route::post('logout', LogoutController::class);
			Route::get('profile', ProfileController::class);
		});
	});

	Route::group(['prefix' => 'users'], function () {
		Route::middleware('auth:sanctum')->group(function () {
			Route::get('vendors', VendorController::class);
		});
	});
	
	Route::group(['prefix' => 'products'], function () {
		Route::resource('categories', CategoryController::class, [
			'as' => 'apiv1'
		]);
    });
	
	Route::resource('members', MemberController::class, [
		'as' => 'apiv1'
	]);
	
});
