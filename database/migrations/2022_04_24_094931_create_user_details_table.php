<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_details', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->unsignedBigInteger('user_id');
            $table->string('company_name', 255);
            $table->string('leader_name', 255);
            $table->string('address', 255);
            $table->string('phone', 20)->nullable()->default(null);
            $table->string('mobile', 20)->nullable()->default(null);
			$table->double('lat')->nullable()->default(0);
			$table->double('lng')->nullable()->default(0);
			$table->string('logo')->nullable()->default(null);
            $table->timestamps();
			$table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_details');
    }
};
