<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transaction_items', function (Blueprint $table) {
			$table->uuid('id')->primary();
            $table->uuid('transaction_id');
            $table->unsignedBigInteger('product_id');
			$table->string('product_name', 200);
			$table->double('qty', 5,);
			$table->bigInteger('price');
			$table->double('total', 15, 2)->default(0);
            $table->timestamps();
			$table->foreign('product_id')->references('id')->on('products')->onDelete('cascade');
			$table->foreign('transaction_id')->references('id')->on('transactions')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transaction_items');
    }
};
