<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->unsignedBigInteger('vendor_id');
            $table->unsignedBigInteger('member_id');
            $table->bigInteger('transaction_number');
            $table->string('ref_number', 50)->nullable()->default(null);
			$table->double('total', 15, 2);
			$table->enum('type', ['D', 'K']);
            $table->timestamps();
			$table->foreign('vendor_id')->references('id')->on('users')->onDelete('cascade');
			$table->foreign('member_id')->references('id')->on('users')->onDelete('cascade');

			$table->unique(["vendor_id", "transaction_number"], 'trans_unique');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
		//Schema::table('transactions', function (Blueprint $table) {
		//	$table->dropUnique('trans_unique');
		//});
        Schema::dropIfExists('transactions');
    }
};
