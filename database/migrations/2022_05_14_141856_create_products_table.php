<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('vendor_id');
            $table->unsignedBigInteger('category_id');
            $table->string('name', 200);
            $table->text('desc', 255)->nullable()->default(null);
            $table->integer('price');
            $table->smallInteger('discount');
            $table->smallInteger('stock')->default(0);
			$table->boolean("is_active")->default(true);
			$table->boolean("is_delete")->default(false);
			$table->string('logo', 255)->nullable()->default(null);
            $table->timestamps();
			$table->foreign('vendor_id')->references('id')->on('users')->onDelete('cascade');
			$table->foreign('category_id')->references('id')->on('categories')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
};
