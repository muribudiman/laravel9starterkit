<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use App\Models\User;

class VendorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $arrayData = [
			[
                'name' => 'Bank Sampah Kalasan',
                'email' => 'joglostudio@gmail.com',
                'password' => Hash::make("12345678"),
                'role' => "vendor",
				'detail' => [
					'company_name' => 'Bank Sampah Kalasan',
					'leader_name' => 'Zohan',
					'address' => 'Jl. Raya Yogya - Solo, Suryatmajan, Danurejan, Daerah Istimewa Yogyakarta',
					'phone' => '0274 123456',
					'mobile' => '0987654321',
					'lat' => '-7.766818518155677',
					'lng' => '110.47264512063761'
				]
			],
			[
                'name' => 'Bank Sampah Asri',
                'email' => 'banksampahasri@gmail.com',
                'password' => Hash::make("12345678"),
                'role' => "vendor",
				'detail' => [
					'company_name' => 'Bank Sampah Asri',
					'leader_name' => 'Ahmad Dhani',
					'address' => 'Gambiran Baru UH/5 53 RT 45 RW 008 Kel Pandeyan Kec Umbulharjo Yogyakarta',
					'phone' => '0274 112233',
					'mobile' => '0987654321',
					'lat' => '-7.81730406708698',
					'lng' => '110.39343651080071',
				]
			],
			[
                'name' => 'Bank Sampah Blazent',
                'email' => 'template1@example.com',
                'password' => Hash::make("12345678"),
                'role' => "vendor",
				'detail' => [
					'company_name' => 'Bank Sampah Blazent',
					'leader_name' => '-',
					'address' => 'Jl. Taman Siswa No.7, Gunungketur, Pakualaman, Kota Yogyakarta, Daerah Istimewa Yogyakarta 55151',
					'phone' => '',
					'mobile' => '',
					'lat' => '-7.80218227529431',
					'lng' => '110.37777703121586',
				]
			],
			[
                'name' => 'Bank Sampah Damai Bersatu',
                'email' => 'template2@example.com',
                'password' => Hash::make("12345678"),
                'role' => "vendor",
				'detail' => [
					'company_name' => 'Bank Sampah Damai Bersatu',
					'leader_name' => '-',
					'address' => 'Jl. Brigjen Katamso No.1188 B, Prawirodirjan, Kec. Gondomanan, Kota Yogyakarta, Daerah Istimewa Yogyakarta 55121',
					'phone' => '',
					'mobile' => '',
					'lat' => '-7.805610702939828',
					'lng' => '110.370180204098921',
				]
			],
			[
                'name' => 'Bank Sampah Tresno Tuhutentrem',
                'email' => 'template3@example.com',
                'password' => Hash::make("12345678"),
                'role' => "vendor",
				'detail' => [
					'company_name' => 'Bank Sampah Tresno Tuhutentrem',
					'leader_name' => '-',
					'address' => 'Jl. Sorosutan No.33, RW.5, Sorosutan, Kec. Umbulharjo, Kota Yogyakarta, Daerah Istimewa Yogyakarta 55162',
					'phone' => '',
					'mobile' => '+6281904260064',
					'lat' => '-7.8203844579403095',
					'lng' => '110.37873197622105',
				]
			],
			[
                'name' => 'Bank Sampah Pelangi',
                'email' => 'template4@example.com',
                'password' => Hash::make("12345678"),
                'role' => "vendor",
				'detail' => [
					'company_name' => 'Bank Sampah Pelangi',
					'leader_name' => '-',
					'address' => 'Jl. H. Agus Salim No.74, Notoprajan, Ngampilan, Kota Yogyakarta, Daerah Istimewa Yogyakarta 55262',
					'phone' => '',
					'mobile' => '',
					'lat' => '-7.80365865056546',
					'lng' => '110.35824223401659',
				]
			],
			[
                'name' => 'Bank Sampah MAKMUR Kotabaru',
                'email' => 'template5@example.com',
                'password' => Hash::make("12345678"),
                'role' => "vendor",
				'detail' => [
					'company_name' => 'Bank Sampah MAKMUR Kotabaru',
					'leader_name' => '-',
					'address' => 'Jl. Atmosukarto No.9, RT.11/RW.03, Kotabaru, Kec. Gondokusuman, Kota Yogyakarta, Daerah Istimewa Yogyakarta 55224',
					'phone' => '',
					'mobile' => '',
					'lat' => '-7.78799823505202',
					'lng' => '110.37626642682022',
				]
			],
			[
                'name' => 'Bank Sampah Surolaras',
                'email' => 'template6@example.com',
                'password' => Hash::make("12345678"),
                'role' => "vendor",
				'detail' => [
					'company_name' => 'Bank Sampah Surolaras',
					'leader_name' => '-',
					'address' => 'Jl. Suronatan No.Blok NG-2/51, Notoprajan, Ngampilan, Kota Yogyakarta, Daerah Istimewa Yogyakarta 55262',
					'phone' => '',
					'mobile' => '',
					'lat' => '-7.801986489888218',
					'lng' => '110.35953788819063',
				]
			],
			[
                'name' => 'Bank Sampah Pilah Pilih Mandiri',
                'email' => 'template7@example.com',
                'password' => Hash::make("12345678"),
                'role' => "vendor",
				'detail' => [
					'company_name' => 'Bank Sampah Pilah Pilih Mandiri',
					'leader_name' => '-',
					'address' => 'Jl. KH Wahid Hasyim No.8, Notoprajan, Ngampilan, Kota Yogyakarta, Daerah Istimewa Yogyakarta 55262',
					'phone' => '',
					'mobile' => '',
					'lat' => '-7.804643861558739',
					'lng' => '110.35614757612429',
				]
			],
			[
                'name' => 'Bank Sampah Resik lan Pikoleh',
                'email' => 'template8@example.com',
                'password' => Hash::make("12345678"),
                'role' => "vendor",
				'detail' => [
					'company_name' => 'Bank Sampah Resik lan Pikoleh',
					'leader_name' => '-',
					'address' => 'Unnamed Road, Suryodiningratan, Kec. Mantrijeron, Kota Yogyakarta, Daerah Istimewa Yogyakarta 55141',
					'phone' => '',
					'mobile' => '',
					'lat' => '-7.814265585910109',
					'lng' => '110.36216083051934',
				]
			],
			[
                'name' => 'Bank Sampah Mekar Melati',
                'email' => 'template9@example.com',
                'password' => Hash::make("12345678"),
                'role' => "vendor",
				'detail' => [
					'company_name' => 'Bank Sampah Mekar Melati',
					'leader_name' => '-',
					'address' => 'Gg. Slamet, Notoprajan, Ngampilan, Kota Yogyakarta, Daerah Istimewa Yogyakarta 55262',
					'phone' => '',
					'mobile' => '',
					'lat' => '-7.802013753855763',
					'lng' => '110.35505498819064',
				]
			],
			[
                'name' => 'Bank Sampah Suryo Resik',
                'email' => 'template10@example.com',
                'password' => Hash::make("12345678"),
                'role' => "vendor",
				'detail' => [
					'company_name' => 'Bank Sampah Suryo Resik',
					'leader_name' => '-',
					'address' => 'Mj 2/822, RT.44/RW.13, Suryodiningratan, Kec. Mantrijeron, Kota Yogyakarta, Daerah Istimewa Yogyakarta 55141',
					'phone' => '',
					'mobile' => '',
					'lat' => '-7.818638949091695',
					'lng' => '110.35915183051935',
				]
			],
			[
                'name' => 'Bank Sampah BERSERI',
                'email' => 'template11@example.com',
                'password' => Hash::make("12345678"),
                'role' => "vendor",
				'detail' => [
					'company_name' => 'Bank Sampah BERSERI',
					'leader_name' => '-',
					'address' => 'Jl. Cik Di Tiro No.5, Terban, Kec. Gondokusuman, Kota Yogyakarta, Daerah Istimewa Yogyakarta 55223',
					'phone' => '',
					'mobile' => '',
					'lat' => '-7.78004922718948',
					'lng' => '110.37453441517673',
				]
			],
			[
                'name' => 'Bank Sampah Sido Resik',
                'email' => 'template12@example.com',
                'password' => Hash::make("12345678"),
                'role' => "vendor",
				'detail' => [
					'company_name' => 'Bank Sampah Sido Resik',
					'leader_name' => '-',
					'address' => 'Jl. Mangkuyudan No.18, Mantrijeron, Kec. Mantrijeron, Kota Yogyakarta, Daerah Istimewa Yogyakarta 55143',
					'phone' => '',
					'mobile' => '',
					'lat' => '-7.82070997717165',
					'lng' => '110.36323485750546',
				]
			],
			[
                'name' => 'Bank Sampah Sorwo',
                'email' => 'template14@example.com',
                'password' => Hash::make("12345678"),
                'role' => "vendor",
				'detail' => [
					'company_name' => 'Bank Sampah Sorwo',
					'leader_name' => '-',
					'address' => 'Jl. Masjid Khusnul Khotimah, Bumijo, Kec. Jetis, Kota Yogyakarta, Daerah Istimewa Yogyakarta 55231',
					'phone' => '',
					'mobile' => '',
					'lat' => '-7.783669031032129',
					'lng' => '110.36269925750543',
				]
			],
			[
                'name' => 'Bank Sampah Pandan Wangi Demangan',
                'email' => 'template15@example.com',
                'password' => Hash::make("12345678"),
                'role' => "vendor",
				'detail' => [
					'company_name' => 'Bank Sampah Pandan Wangi Demangan',
					'leader_name' => '-',
					'address' => 'Jl. Nangka No.584, Demangan, Kec. Gondokusuman, Kota Yogyakarta, Daerah Istimewa Yogyakarta 55221',
					'phone' => '',
					'mobile' => '',
					'lat' => '-7.785045271096105',
					'lng' => '110.38970765750544',
				]
			],
			[
                'name' => 'Banks Sampah Reresik',
                'email' => 'template16@example.com',
                'password' => Hash::make("12345678"),
                'role' => "vendor",
				'detail' => [
					'company_name' => 'Banks Sampah Reresik',
					'leader_name' => '-',
					'address' => 'Jl. Prawirotaman No.21 B, Brontokusuman, Kec. Mergangsan, Kota Yogyakarta, Daerah Istimewa Yogyakarta 55153',
					'phone' => '',
					'mobile' => '',
					'lat' => '-7.8185852326892915',
					'lng' => '110.37070159983413',
				]
			],
			[
                'name' => 'Bank Sampah Pokoke Resik',
                'email' => 'template17@example.com',
                'password' => Hash::make("12345678"),
                'role' => "vendor",
				'detail' => [
					'company_name' => 'Bank Sampah Pokoke Resik',
					'leader_name' => '-',
					'address' => 'Gg. Darusalam No.602, Notoprajan, Ngampilan, Kota Yogyakarta, Daerah Istimewa Yogyakarta 55262',
					'phone' => '',
					'mobile' => '',
					'lat' => '-7.801672934255056',
					'lng' => '110.35734745750544',
				]
			],
			[
                'name' => 'Bank Sampah Barokah Daleman Prenggan',
                'email' => 'template18@example.com',
                'password' => Hash::make("12345678"),
                'role' => "vendor",
				'detail' => [
					'company_name' => 'Bank Sampah Barokah Daleman Prenggan',
					'leader_name' => '-',
					'address' => 'Gg. Gemah Ripah, Purbayan, Kec. Kotagede, Kota Yogyakarta, Daerah Istimewa Yogyakarta 55173',
					'phone' => '',
					'mobile' => '',
					'lat' => '-7.831473113692687',
					'lng' => '110.40009185750543',
				]
			],
			[
                'name' => 'Bank Sampah Serangan Kudu Resik',
                'email' => 'template19@example.com',
                'password' => Hash::make("12345678"),
                'role' => "vendor",
				'detail' => [
					'company_name' => 'Bank Sampah Serangan Kudu Resik',
					'leader_name' => '-',
					'address' => 'Serangan NG II 216, RT.14/RW.02, Notoprajan, Ngampilan, Kota Yogyakarta, Daerah Istimewa Yogyakarta 55262',
					'phone' => '',
					'mobile' => '',
					'lat' => '-7.803713970523588',
					'lng' => '110.35589838449151',
				]
			],
			[
                'name' => 'Bank Sampah Mondoroko RW 7',
                'email' => 'template20@example.com',
                'password' => Hash::make("12345678"),
                'role' => "vendor",
				'detail' => [
					'company_name' => 'Bank Sampah Mondoroko RW 7',
					'leader_name' => '-',
					'address' => 'Jl. Mondorakan No.27, Prenggan, Kec. Kotagede, Kota Yogyakarta, Daerah Istimewa Yogyakarta 55172',
					'phone' => '',
					'mobile' => '+6281804320636',
					'lat' => '-7.827177040099077',
					'lng' => '110.39632664216282',
				]
			],

        ];
        
        foreach($arrayData as $key => $value) {
			$detail = [];
			if(isset($value['detail'])) {
				$detail = $value['detail'];
				unset($value['detail']);
			}

			if($model = User::create($value)) {
				$model->user_detail()->create($detail);
			}
        }
    }
}
