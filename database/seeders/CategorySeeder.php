<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Category;
use App\Models\Product;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $arrayData = [
			[
				'name' => 'Kertas',
				'desc' => 'Kategori Semua Kertas',
				'products' => [
					[
						'vendor_id' => 3,
						'name' => 'Kertas Koran',
						'desc' => 'Kertas Koran',
						'price' => 2500,
						'discount' => 0,
						'stock' => 0,
					],
					[
						'vendor_id' => 3,
						'name' => 'Kantong Semen',
						'desc' => 'Kertas Semen',
						'price' => 4200,
						'discount' => 0,
						'stock' => 0,
					]
				]
			],
			[
				'name' => 'Logam',
				'desc' => 'Kategori Semua Logam',
				'products' => [
					[
						'vendor_id' => 3,
						'name' => 'Aluminium',
						'desc' => 'Aluminium',
						'price' => 15000,
						'discount' => 0,
						'stock' => 0,
					],
					[
						'vendor_id' => 3,
						'name' => 'Besi Tua Kelas A',
						'desc' => 'Besi Tua Kelas A',
						'price' => 5200,
						'discount' => 0,
						'stock' => 0,
					],
					[
						'vendor_id' => 3,
						'name' => 'Besi Tua Kelas B',
						'desc' => 'Besi Tua Kelas B',
						'price' => 5100,
						'discount' => 0,
						'stock' => 0,
					],
					[
						'vendor_id' => 3,
						'name' => 'Besi Tua Kelas C',
						'desc' => 'Besi Tua Kelas C',
						'price' => 5000,
						'discount' => 0,
						'stock' => 0,
					]
				]
			],
			[
				'name' => 'Plastik',
				'desc' => 'Kategori Semua Plastik',
				'products' => [
					[
						'vendor_id' => 3,
						'name' => 'Plastik Bening Berbentuk Gelas',
						'desc' => 'Plastik Bening Berbentuk Gelas',
						'price' => 3000,
						'discount' => 0,
						'stock' => 0,
					],
					[
						'vendor_id' => 3,
						'name' => 'Botol plastik',
						'desc' => 'Botol plastik',
						'price' => 2500,
						'discount' => 0,
						'stock' => 0,
					]
				]
			],
		];

		foreach($arrayData as $value) {
			$products = $value['products'];
			unset($value['products']);
			if($model = Category::create($value)) {
				$model->save();
				foreach($products as $val) {
					$val['category_id'] = $model->id;
					Product::create($val);
				}
			}

			
		}
		
    }
}
