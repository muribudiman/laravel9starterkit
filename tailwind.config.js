const defaultTheme = require('tailwindcss/defaultTheme');

module.exports = {
    content: [
        './vendor/laravel/framework/src/Illuminate/Pagination/resources/views/*.blade.php',
        './vendor/laravel/jetstream/**/*.blade.php',
        './storage/framework/views/*.php',
        './resources/views/**/*.blade.php',
        './resources/js/**/*.vue',
    ],

    //theme: {
    //    extend: {
    //        fontFamily: {
    //            sans: ['Nunito', ...defaultTheme.fontFamily.sans],
    //        },
    //    },
    //},

    theme: {
        extend: {
            fontFamily: {
                base: ['Poppins', 'sans-serif'],
            },
            zIndex: {
                '-1': '-1'
            },
            flexGrow: {
                5: '5'
            },
            maxHeight: {
                'screen-menu': 'calc(100vh - 3.5rem)',
                modal: 'calc(100vh - 160px)'
            },
            transitionProperty: {
                position: 'right, left, top, bottom, margin, padding',
                textColor: 'color'
            },
            keyframes: {
                fadeOut: {
                    from: { opacity: 1 },
                    to: { opacity: 0 }
                },
                fadeIn: {
                    from: { opacity: 0 },
                    to: { opacity: 1 }
                }
            },
            animation: {
                fadeOut: 'fadeOut 250ms ease-in-out',
                fadeIn: 'fadeIn 250ms ease-in-out'
            },
            screens: {
                '3xl': '1600px',
            },
            colors: {
                primary: {
                    50: '#F7F6FD',
                    100: '#EEEEFB',
                    200: '#D5D4F5',
                    300: '#BCB9EF',
                    400: '#8A85E4',
                    500: '#5851D8',
                    600: '#4F49C2',
                    700: '#353182',
                    800: '#282461',
                    900: '#1A1841',
                },
                black: '#040405',
            },
        },
    },

    plugins: [require('@tailwindcss/forms'), require('@tailwindcss/typography')],
};